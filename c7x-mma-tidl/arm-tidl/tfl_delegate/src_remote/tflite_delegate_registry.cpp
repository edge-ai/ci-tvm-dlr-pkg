#include <op_registry.h>
#include <packet.h>

#include "tflite_delegate_messages.h"

REGISTER_MSG(tflite_delegate_create_exec_delegate);
REGISTER_MSG(tflite_delegate_delete_exec_delegate);
