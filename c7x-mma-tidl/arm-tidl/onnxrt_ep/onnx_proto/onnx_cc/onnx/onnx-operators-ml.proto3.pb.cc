// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: onnx/onnx-operators-ml.proto3

#include "onnx/onnx-operators-ml.proto3.pb.h"

#include <algorithm>

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/wire_format_lite.h>
#include <google/protobuf/io/zero_copy_stream_impl_lite.h>
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>

PROTOBUF_PRAGMA_INIT_SEG

namespace _pb = ::PROTOBUF_NAMESPACE_ID;
namespace _pbi = _pb::internal;

namespace onnx {
PROTOBUF_CONSTEXPR OperatorProto::OperatorProto(
    ::_pbi::ConstantInitialized)
  : op_type_(&::_pbi::fixed_address_empty_string, ::_pbi::ConstantInitialized{})
  , doc_string_(&::_pbi::fixed_address_empty_string, ::_pbi::ConstantInitialized{})
  , since_version_(int64_t{0})
  , status_(0)
{}
struct OperatorProtoDefaultTypeInternal {
  PROTOBUF_CONSTEXPR OperatorProtoDefaultTypeInternal()
      : _instance(::_pbi::ConstantInitialized{}) {}
  ~OperatorProtoDefaultTypeInternal() {}
  union {
    OperatorProto _instance;
  };
};
PROTOBUF_ATTRIBUTE_NO_DESTROY PROTOBUF_CONSTINIT PROTOBUF_ATTRIBUTE_INIT_PRIORITY1 OperatorProtoDefaultTypeInternal _OperatorProto_default_instance_;
PROTOBUF_CONSTEXPR OperatorSetProto::OperatorSetProto(
    ::_pbi::ConstantInitialized)
  : operator__()
  , functions_()
  , magic_(&::_pbi::fixed_address_empty_string, ::_pbi::ConstantInitialized{})
  , ir_version_prerelease_(&::_pbi::fixed_address_empty_string, ::_pbi::ConstantInitialized{})
  , domain_(&::_pbi::fixed_address_empty_string, ::_pbi::ConstantInitialized{})
  , doc_string_(&::_pbi::fixed_address_empty_string, ::_pbi::ConstantInitialized{})
  , ir_build_metadata_(&::_pbi::fixed_address_empty_string, ::_pbi::ConstantInitialized{})
  , ir_version_(int64_t{0})
  , opset_version_(int64_t{0}){}
struct OperatorSetProtoDefaultTypeInternal {
  PROTOBUF_CONSTEXPR OperatorSetProtoDefaultTypeInternal()
      : _instance(::_pbi::ConstantInitialized{}) {}
  ~OperatorSetProtoDefaultTypeInternal() {}
  union {
    OperatorSetProto _instance;
  };
};
PROTOBUF_ATTRIBUTE_NO_DESTROY PROTOBUF_CONSTINIT PROTOBUF_ATTRIBUTE_INIT_PRIORITY1 OperatorSetProtoDefaultTypeInternal _OperatorSetProto_default_instance_;
}  // namespace onnx
namespace onnx {

// ===================================================================

class OperatorProto::_Internal {
 public:
};

OperatorProto::OperatorProto(::PROTOBUF_NAMESPACE_ID::Arena* arena,
                         bool is_message_owned)
  : ::PROTOBUF_NAMESPACE_ID::MessageLite(arena, is_message_owned) {
  SharedCtor();
  // @@protoc_insertion_point(arena_constructor:onnx.OperatorProto)
}
OperatorProto::OperatorProto(const OperatorProto& from)
  : ::PROTOBUF_NAMESPACE_ID::MessageLite() {
  _internal_metadata_.MergeFrom<std::string>(from._internal_metadata_);
  op_type_.InitDefault();
  #ifdef PROTOBUF_FORCE_COPY_DEFAULT_STRING
    op_type_.Set("", GetArenaForAllocation());
  #endif // PROTOBUF_FORCE_COPY_DEFAULT_STRING
  if (!from._internal_op_type().empty()) {
    op_type_.Set(from._internal_op_type(), 
      GetArenaForAllocation());
  }
  doc_string_.InitDefault();
  #ifdef PROTOBUF_FORCE_COPY_DEFAULT_STRING
    doc_string_.Set("", GetArenaForAllocation());
  #endif // PROTOBUF_FORCE_COPY_DEFAULT_STRING
  if (!from._internal_doc_string().empty()) {
    doc_string_.Set(from._internal_doc_string(), 
      GetArenaForAllocation());
  }
  ::memcpy(&since_version_, &from.since_version_,
    static_cast<size_t>(reinterpret_cast<char*>(&status_) -
    reinterpret_cast<char*>(&since_version_)) + sizeof(status_));
  // @@protoc_insertion_point(copy_constructor:onnx.OperatorProto)
}

inline void OperatorProto::SharedCtor() {
op_type_.InitDefault();
#ifdef PROTOBUF_FORCE_COPY_DEFAULT_STRING
  op_type_.Set("", GetArenaForAllocation());
#endif // PROTOBUF_FORCE_COPY_DEFAULT_STRING
doc_string_.InitDefault();
#ifdef PROTOBUF_FORCE_COPY_DEFAULT_STRING
  doc_string_.Set("", GetArenaForAllocation());
#endif // PROTOBUF_FORCE_COPY_DEFAULT_STRING
::memset(reinterpret_cast<char*>(this) + static_cast<size_t>(
    reinterpret_cast<char*>(&since_version_) - reinterpret_cast<char*>(this)),
    0, static_cast<size_t>(reinterpret_cast<char*>(&status_) -
    reinterpret_cast<char*>(&since_version_)) + sizeof(status_));
}

OperatorProto::~OperatorProto() {
  // @@protoc_insertion_point(destructor:onnx.OperatorProto)
  if (auto *arena = _internal_metadata_.DeleteReturnArena<std::string>()) {
  (void)arena;
    return;
  }
  SharedDtor();
}

inline void OperatorProto::SharedDtor() {
  GOOGLE_DCHECK(GetArenaForAllocation() == nullptr);
  op_type_.Destroy();
  doc_string_.Destroy();
}

void OperatorProto::SetCachedSize(int size) const {
  _cached_size_.Set(size);
}

void OperatorProto::Clear() {
// @@protoc_insertion_point(message_clear_start:onnx.OperatorProto)
  uint32_t cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  op_type_.ClearToEmpty();
  doc_string_.ClearToEmpty();
  ::memset(&since_version_, 0, static_cast<size_t>(
      reinterpret_cast<char*>(&status_) -
      reinterpret_cast<char*>(&since_version_)) + sizeof(status_));
  _internal_metadata_.Clear<std::string>();
}

const char* OperatorProto::_InternalParse(const char* ptr, ::_pbi::ParseContext* ctx) {
#define CHK_(x) if (PROTOBUF_PREDICT_FALSE(!(x))) goto failure
  while (!ctx->Done(&ptr)) {
    uint32_t tag;
    ptr = ::_pbi::ReadTag(ptr, &tag);
    switch (tag >> 3) {
      // string op_type = 1;
      case 1:
        if (PROTOBUF_PREDICT_TRUE(static_cast<uint8_t>(tag) == 10)) {
          auto str = _internal_mutable_op_type();
          ptr = ::_pbi::InlineGreedyStringParser(str, ptr, ctx);
          CHK_(ptr);
          CHK_(::_pbi::VerifyUTF8(str, nullptr));
        } else
          goto handle_unusual;
        continue;
      // int64 since_version = 2;
      case 2:
        if (PROTOBUF_PREDICT_TRUE(static_cast<uint8_t>(tag) == 16)) {
          since_version_ = ::PROTOBUF_NAMESPACE_ID::internal::ReadVarint64(&ptr);
          CHK_(ptr);
        } else
          goto handle_unusual;
        continue;
      // .onnx.OperatorStatus status = 3;
      case 3:
        if (PROTOBUF_PREDICT_TRUE(static_cast<uint8_t>(tag) == 24)) {
          uint64_t val = ::PROTOBUF_NAMESPACE_ID::internal::ReadVarint64(&ptr);
          CHK_(ptr);
          _internal_set_status(static_cast<::onnx::OperatorStatus>(val));
        } else
          goto handle_unusual;
        continue;
      // string doc_string = 10;
      case 10:
        if (PROTOBUF_PREDICT_TRUE(static_cast<uint8_t>(tag) == 82)) {
          auto str = _internal_mutable_doc_string();
          ptr = ::_pbi::InlineGreedyStringParser(str, ptr, ctx);
          CHK_(ptr);
          CHK_(::_pbi::VerifyUTF8(str, nullptr));
        } else
          goto handle_unusual;
        continue;
      default:
        goto handle_unusual;
    }  // switch
  handle_unusual:
    if ((tag == 0) || ((tag & 7) == 4)) {
      CHK_(ptr);
      ctx->SetLastTag(tag);
      goto message_done;
    }
    ptr = UnknownFieldParse(
        tag,
        _internal_metadata_.mutable_unknown_fields<std::string>(),
        ptr, ctx);
    CHK_(ptr != nullptr);
  }  // while
message_done:
  return ptr;
failure:
  ptr = nullptr;
  goto message_done;
#undef CHK_
}

uint8_t* OperatorProto::_InternalSerialize(
    uint8_t* target, ::PROTOBUF_NAMESPACE_ID::io::EpsCopyOutputStream* stream) const {
  // @@protoc_insertion_point(serialize_to_array_start:onnx.OperatorProto)
  uint32_t cached_has_bits = 0;
  (void) cached_has_bits;

  // string op_type = 1;
  if (!this->_internal_op_type().empty()) {
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::VerifyUtf8String(
      this->_internal_op_type().data(), static_cast<int>(this->_internal_op_type().length()),
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::SERIALIZE,
      "onnx.OperatorProto.op_type");
    target = stream->WriteStringMaybeAliased(
        1, this->_internal_op_type(), target);
  }

  // int64 since_version = 2;
  if (this->_internal_since_version() != 0) {
    target = stream->EnsureSpace(target);
    target = ::_pbi::WireFormatLite::WriteInt64ToArray(2, this->_internal_since_version(), target);
  }

  // .onnx.OperatorStatus status = 3;
  if (this->_internal_status() != 0) {
    target = stream->EnsureSpace(target);
    target = ::_pbi::WireFormatLite::WriteEnumToArray(
      3, this->_internal_status(), target);
  }

  // string doc_string = 10;
  if (!this->_internal_doc_string().empty()) {
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::VerifyUtf8String(
      this->_internal_doc_string().data(), static_cast<int>(this->_internal_doc_string().length()),
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::SERIALIZE,
      "onnx.OperatorProto.doc_string");
    target = stream->WriteStringMaybeAliased(
        10, this->_internal_doc_string(), target);
  }

  if (PROTOBUF_PREDICT_FALSE(_internal_metadata_.have_unknown_fields())) {
    target = stream->WriteRaw(_internal_metadata_.unknown_fields<std::string>(::PROTOBUF_NAMESPACE_ID::internal::GetEmptyString).data(),
        static_cast<int>(_internal_metadata_.unknown_fields<std::string>(::PROTOBUF_NAMESPACE_ID::internal::GetEmptyString).size()), target);
  }
  // @@protoc_insertion_point(serialize_to_array_end:onnx.OperatorProto)
  return target;
}

size_t OperatorProto::ByteSizeLong() const {
// @@protoc_insertion_point(message_byte_size_start:onnx.OperatorProto)
  size_t total_size = 0;

  uint32_t cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  // string op_type = 1;
  if (!this->_internal_op_type().empty()) {
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::StringSize(
        this->_internal_op_type());
  }

  // string doc_string = 10;
  if (!this->_internal_doc_string().empty()) {
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::StringSize(
        this->_internal_doc_string());
  }

  // int64 since_version = 2;
  if (this->_internal_since_version() != 0) {
    total_size += ::_pbi::WireFormatLite::Int64SizePlusOne(this->_internal_since_version());
  }

  // .onnx.OperatorStatus status = 3;
  if (this->_internal_status() != 0) {
    total_size += 1 +
      ::_pbi::WireFormatLite::EnumSize(this->_internal_status());
  }

  if (PROTOBUF_PREDICT_FALSE(_internal_metadata_.have_unknown_fields())) {
    total_size += _internal_metadata_.unknown_fields<std::string>(::PROTOBUF_NAMESPACE_ID::internal::GetEmptyString).size();
  }
  int cached_size = ::_pbi::ToCachedSize(total_size);
  SetCachedSize(cached_size);
  return total_size;
}

void OperatorProto::CheckTypeAndMergeFrom(
    const ::PROTOBUF_NAMESPACE_ID::MessageLite& from) {
  MergeFrom(*::_pbi::DownCast<const OperatorProto*>(
      &from));
}

void OperatorProto::MergeFrom(const OperatorProto& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:onnx.OperatorProto)
  GOOGLE_DCHECK_NE(&from, this);
  uint32_t cached_has_bits = 0;
  (void) cached_has_bits;

  if (!from._internal_op_type().empty()) {
    _internal_set_op_type(from._internal_op_type());
  }
  if (!from._internal_doc_string().empty()) {
    _internal_set_doc_string(from._internal_doc_string());
  }
  if (from._internal_since_version() != 0) {
    _internal_set_since_version(from._internal_since_version());
  }
  if (from._internal_status() != 0) {
    _internal_set_status(from._internal_status());
  }
  _internal_metadata_.MergeFrom<std::string>(from._internal_metadata_);
}

void OperatorProto::CopyFrom(const OperatorProto& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:onnx.OperatorProto)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool OperatorProto::IsInitialized() const {
  return true;
}

void OperatorProto::InternalSwap(OperatorProto* other) {
  using std::swap;
  auto* lhs_arena = GetArenaForAllocation();
  auto* rhs_arena = other->GetArenaForAllocation();
  _internal_metadata_.InternalSwap(&other->_internal_metadata_);
  ::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr::InternalSwap(
      &op_type_, lhs_arena,
      &other->op_type_, rhs_arena
  );
  ::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr::InternalSwap(
      &doc_string_, lhs_arena,
      &other->doc_string_, rhs_arena
  );
  ::PROTOBUF_NAMESPACE_ID::internal::memswap<
      PROTOBUF_FIELD_OFFSET(OperatorProto, status_)
      + sizeof(OperatorProto::status_)
      - PROTOBUF_FIELD_OFFSET(OperatorProto, since_version_)>(
          reinterpret_cast<char*>(&since_version_),
          reinterpret_cast<char*>(&other->since_version_));
}

std::string OperatorProto::GetTypeName() const {
  return "onnx.OperatorProto";
}


// ===================================================================

class OperatorSetProto::_Internal {
 public:
};

void OperatorSetProto::clear_functions() {
  functions_.Clear();
}
OperatorSetProto::OperatorSetProto(::PROTOBUF_NAMESPACE_ID::Arena* arena,
                         bool is_message_owned)
  : ::PROTOBUF_NAMESPACE_ID::MessageLite(arena, is_message_owned),
  operator__(arena),
  functions_(arena) {
  SharedCtor();
  // @@protoc_insertion_point(arena_constructor:onnx.OperatorSetProto)
}
OperatorSetProto::OperatorSetProto(const OperatorSetProto& from)
  : ::PROTOBUF_NAMESPACE_ID::MessageLite(),
      operator__(from.operator__),
      functions_(from.functions_) {
  _internal_metadata_.MergeFrom<std::string>(from._internal_metadata_);
  magic_.InitDefault();
  #ifdef PROTOBUF_FORCE_COPY_DEFAULT_STRING
    magic_.Set("", GetArenaForAllocation());
  #endif // PROTOBUF_FORCE_COPY_DEFAULT_STRING
  if (!from._internal_magic().empty()) {
    magic_.Set(from._internal_magic(), 
      GetArenaForAllocation());
  }
  ir_version_prerelease_.InitDefault();
  #ifdef PROTOBUF_FORCE_COPY_DEFAULT_STRING
    ir_version_prerelease_.Set("", GetArenaForAllocation());
  #endif // PROTOBUF_FORCE_COPY_DEFAULT_STRING
  if (!from._internal_ir_version_prerelease().empty()) {
    ir_version_prerelease_.Set(from._internal_ir_version_prerelease(), 
      GetArenaForAllocation());
  }
  domain_.InitDefault();
  #ifdef PROTOBUF_FORCE_COPY_DEFAULT_STRING
    domain_.Set("", GetArenaForAllocation());
  #endif // PROTOBUF_FORCE_COPY_DEFAULT_STRING
  if (!from._internal_domain().empty()) {
    domain_.Set(from._internal_domain(), 
      GetArenaForAllocation());
  }
  doc_string_.InitDefault();
  #ifdef PROTOBUF_FORCE_COPY_DEFAULT_STRING
    doc_string_.Set("", GetArenaForAllocation());
  #endif // PROTOBUF_FORCE_COPY_DEFAULT_STRING
  if (!from._internal_doc_string().empty()) {
    doc_string_.Set(from._internal_doc_string(), 
      GetArenaForAllocation());
  }
  ir_build_metadata_.InitDefault();
  #ifdef PROTOBUF_FORCE_COPY_DEFAULT_STRING
    ir_build_metadata_.Set("", GetArenaForAllocation());
  #endif // PROTOBUF_FORCE_COPY_DEFAULT_STRING
  if (!from._internal_ir_build_metadata().empty()) {
    ir_build_metadata_.Set(from._internal_ir_build_metadata(), 
      GetArenaForAllocation());
  }
  ::memcpy(&ir_version_, &from.ir_version_,
    static_cast<size_t>(reinterpret_cast<char*>(&opset_version_) -
    reinterpret_cast<char*>(&ir_version_)) + sizeof(opset_version_));
  // @@protoc_insertion_point(copy_constructor:onnx.OperatorSetProto)
}

inline void OperatorSetProto::SharedCtor() {
magic_.InitDefault();
#ifdef PROTOBUF_FORCE_COPY_DEFAULT_STRING
  magic_.Set("", GetArenaForAllocation());
#endif // PROTOBUF_FORCE_COPY_DEFAULT_STRING
ir_version_prerelease_.InitDefault();
#ifdef PROTOBUF_FORCE_COPY_DEFAULT_STRING
  ir_version_prerelease_.Set("", GetArenaForAllocation());
#endif // PROTOBUF_FORCE_COPY_DEFAULT_STRING
domain_.InitDefault();
#ifdef PROTOBUF_FORCE_COPY_DEFAULT_STRING
  domain_.Set("", GetArenaForAllocation());
#endif // PROTOBUF_FORCE_COPY_DEFAULT_STRING
doc_string_.InitDefault();
#ifdef PROTOBUF_FORCE_COPY_DEFAULT_STRING
  doc_string_.Set("", GetArenaForAllocation());
#endif // PROTOBUF_FORCE_COPY_DEFAULT_STRING
ir_build_metadata_.InitDefault();
#ifdef PROTOBUF_FORCE_COPY_DEFAULT_STRING
  ir_build_metadata_.Set("", GetArenaForAllocation());
#endif // PROTOBUF_FORCE_COPY_DEFAULT_STRING
::memset(reinterpret_cast<char*>(this) + static_cast<size_t>(
    reinterpret_cast<char*>(&ir_version_) - reinterpret_cast<char*>(this)),
    0, static_cast<size_t>(reinterpret_cast<char*>(&opset_version_) -
    reinterpret_cast<char*>(&ir_version_)) + sizeof(opset_version_));
}

OperatorSetProto::~OperatorSetProto() {
  // @@protoc_insertion_point(destructor:onnx.OperatorSetProto)
  if (auto *arena = _internal_metadata_.DeleteReturnArena<std::string>()) {
  (void)arena;
    return;
  }
  SharedDtor();
}

inline void OperatorSetProto::SharedDtor() {
  GOOGLE_DCHECK(GetArenaForAllocation() == nullptr);
  magic_.Destroy();
  ir_version_prerelease_.Destroy();
  domain_.Destroy();
  doc_string_.Destroy();
  ir_build_metadata_.Destroy();
}

void OperatorSetProto::SetCachedSize(int size) const {
  _cached_size_.Set(size);
}

void OperatorSetProto::Clear() {
// @@protoc_insertion_point(message_clear_start:onnx.OperatorSetProto)
  uint32_t cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  operator__.Clear();
  functions_.Clear();
  magic_.ClearToEmpty();
  ir_version_prerelease_.ClearToEmpty();
  domain_.ClearToEmpty();
  doc_string_.ClearToEmpty();
  ir_build_metadata_.ClearToEmpty();
  ::memset(&ir_version_, 0, static_cast<size_t>(
      reinterpret_cast<char*>(&opset_version_) -
      reinterpret_cast<char*>(&ir_version_)) + sizeof(opset_version_));
  _internal_metadata_.Clear<std::string>();
}

const char* OperatorSetProto::_InternalParse(const char* ptr, ::_pbi::ParseContext* ctx) {
#define CHK_(x) if (PROTOBUF_PREDICT_FALSE(!(x))) goto failure
  while (!ctx->Done(&ptr)) {
    uint32_t tag;
    ptr = ::_pbi::ReadTag(ptr, &tag);
    switch (tag >> 3) {
      // string magic = 1;
      case 1:
        if (PROTOBUF_PREDICT_TRUE(static_cast<uint8_t>(tag) == 10)) {
          auto str = _internal_mutable_magic();
          ptr = ::_pbi::InlineGreedyStringParser(str, ptr, ctx);
          CHK_(ptr);
          CHK_(::_pbi::VerifyUTF8(str, nullptr));
        } else
          goto handle_unusual;
        continue;
      // int64 ir_version = 2;
      case 2:
        if (PROTOBUF_PREDICT_TRUE(static_cast<uint8_t>(tag) == 16)) {
          ir_version_ = ::PROTOBUF_NAMESPACE_ID::internal::ReadVarint64(&ptr);
          CHK_(ptr);
        } else
          goto handle_unusual;
        continue;
      // string ir_version_prerelease = 3;
      case 3:
        if (PROTOBUF_PREDICT_TRUE(static_cast<uint8_t>(tag) == 26)) {
          auto str = _internal_mutable_ir_version_prerelease();
          ptr = ::_pbi::InlineGreedyStringParser(str, ptr, ctx);
          CHK_(ptr);
          CHK_(::_pbi::VerifyUTF8(str, nullptr));
        } else
          goto handle_unusual;
        continue;
      // string domain = 4;
      case 4:
        if (PROTOBUF_PREDICT_TRUE(static_cast<uint8_t>(tag) == 34)) {
          auto str = _internal_mutable_domain();
          ptr = ::_pbi::InlineGreedyStringParser(str, ptr, ctx);
          CHK_(ptr);
          CHK_(::_pbi::VerifyUTF8(str, nullptr));
        } else
          goto handle_unusual;
        continue;
      // int64 opset_version = 5;
      case 5:
        if (PROTOBUF_PREDICT_TRUE(static_cast<uint8_t>(tag) == 40)) {
          opset_version_ = ::PROTOBUF_NAMESPACE_ID::internal::ReadVarint64(&ptr);
          CHK_(ptr);
        } else
          goto handle_unusual;
        continue;
      // string doc_string = 6;
      case 6:
        if (PROTOBUF_PREDICT_TRUE(static_cast<uint8_t>(tag) == 50)) {
          auto str = _internal_mutable_doc_string();
          ptr = ::_pbi::InlineGreedyStringParser(str, ptr, ctx);
          CHK_(ptr);
          CHK_(::_pbi::VerifyUTF8(str, nullptr));
        } else
          goto handle_unusual;
        continue;
      // string ir_build_metadata = 7;
      case 7:
        if (PROTOBUF_PREDICT_TRUE(static_cast<uint8_t>(tag) == 58)) {
          auto str = _internal_mutable_ir_build_metadata();
          ptr = ::_pbi::InlineGreedyStringParser(str, ptr, ctx);
          CHK_(ptr);
          CHK_(::_pbi::VerifyUTF8(str, nullptr));
        } else
          goto handle_unusual;
        continue;
      // repeated .onnx.OperatorProto operator = 8;
      case 8:
        if (PROTOBUF_PREDICT_TRUE(static_cast<uint8_t>(tag) == 66)) {
          ptr -= 1;
          do {
            ptr += 1;
            ptr = ctx->ParseMessage(_internal_add_operator_(), ptr);
            CHK_(ptr);
            if (!ctx->DataAvailable(ptr)) break;
          } while (::PROTOBUF_NAMESPACE_ID::internal::ExpectTag<66>(ptr));
        } else
          goto handle_unusual;
        continue;
      // repeated .onnx.FunctionProto functions = 9;
      case 9:
        if (PROTOBUF_PREDICT_TRUE(static_cast<uint8_t>(tag) == 74)) {
          ptr -= 1;
          do {
            ptr += 1;
            ptr = ctx->ParseMessage(_internal_add_functions(), ptr);
            CHK_(ptr);
            if (!ctx->DataAvailable(ptr)) break;
          } while (::PROTOBUF_NAMESPACE_ID::internal::ExpectTag<74>(ptr));
        } else
          goto handle_unusual;
        continue;
      default:
        goto handle_unusual;
    }  // switch
  handle_unusual:
    if ((tag == 0) || ((tag & 7) == 4)) {
      CHK_(ptr);
      ctx->SetLastTag(tag);
      goto message_done;
    }
    ptr = UnknownFieldParse(
        tag,
        _internal_metadata_.mutable_unknown_fields<std::string>(),
        ptr, ctx);
    CHK_(ptr != nullptr);
  }  // while
message_done:
  return ptr;
failure:
  ptr = nullptr;
  goto message_done;
#undef CHK_
}

uint8_t* OperatorSetProto::_InternalSerialize(
    uint8_t* target, ::PROTOBUF_NAMESPACE_ID::io::EpsCopyOutputStream* stream) const {
  // @@protoc_insertion_point(serialize_to_array_start:onnx.OperatorSetProto)
  uint32_t cached_has_bits = 0;
  (void) cached_has_bits;

  // string magic = 1;
  if (!this->_internal_magic().empty()) {
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::VerifyUtf8String(
      this->_internal_magic().data(), static_cast<int>(this->_internal_magic().length()),
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::SERIALIZE,
      "onnx.OperatorSetProto.magic");
    target = stream->WriteStringMaybeAliased(
        1, this->_internal_magic(), target);
  }

  // int64 ir_version = 2;
  if (this->_internal_ir_version() != 0) {
    target = stream->EnsureSpace(target);
    target = ::_pbi::WireFormatLite::WriteInt64ToArray(2, this->_internal_ir_version(), target);
  }

  // string ir_version_prerelease = 3;
  if (!this->_internal_ir_version_prerelease().empty()) {
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::VerifyUtf8String(
      this->_internal_ir_version_prerelease().data(), static_cast<int>(this->_internal_ir_version_prerelease().length()),
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::SERIALIZE,
      "onnx.OperatorSetProto.ir_version_prerelease");
    target = stream->WriteStringMaybeAliased(
        3, this->_internal_ir_version_prerelease(), target);
  }

  // string domain = 4;
  if (!this->_internal_domain().empty()) {
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::VerifyUtf8String(
      this->_internal_domain().data(), static_cast<int>(this->_internal_domain().length()),
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::SERIALIZE,
      "onnx.OperatorSetProto.domain");
    target = stream->WriteStringMaybeAliased(
        4, this->_internal_domain(), target);
  }

  // int64 opset_version = 5;
  if (this->_internal_opset_version() != 0) {
    target = stream->EnsureSpace(target);
    target = ::_pbi::WireFormatLite::WriteInt64ToArray(5, this->_internal_opset_version(), target);
  }

  // string doc_string = 6;
  if (!this->_internal_doc_string().empty()) {
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::VerifyUtf8String(
      this->_internal_doc_string().data(), static_cast<int>(this->_internal_doc_string().length()),
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::SERIALIZE,
      "onnx.OperatorSetProto.doc_string");
    target = stream->WriteStringMaybeAliased(
        6, this->_internal_doc_string(), target);
  }

  // string ir_build_metadata = 7;
  if (!this->_internal_ir_build_metadata().empty()) {
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::VerifyUtf8String(
      this->_internal_ir_build_metadata().data(), static_cast<int>(this->_internal_ir_build_metadata().length()),
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::SERIALIZE,
      "onnx.OperatorSetProto.ir_build_metadata");
    target = stream->WriteStringMaybeAliased(
        7, this->_internal_ir_build_metadata(), target);
  }

  // repeated .onnx.OperatorProto operator = 8;
  for (unsigned i = 0,
      n = static_cast<unsigned>(this->_internal_operator__size()); i < n; i++) {
    const auto& repfield = this->_internal_operator_(i);
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::
        InternalWriteMessage(8, repfield, repfield.GetCachedSize(), target, stream);
  }

  // repeated .onnx.FunctionProto functions = 9;
  for (unsigned i = 0,
      n = static_cast<unsigned>(this->_internal_functions_size()); i < n; i++) {
    const auto& repfield = this->_internal_functions(i);
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::
        InternalWriteMessage(9, repfield, repfield.GetCachedSize(), target, stream);
  }

  if (PROTOBUF_PREDICT_FALSE(_internal_metadata_.have_unknown_fields())) {
    target = stream->WriteRaw(_internal_metadata_.unknown_fields<std::string>(::PROTOBUF_NAMESPACE_ID::internal::GetEmptyString).data(),
        static_cast<int>(_internal_metadata_.unknown_fields<std::string>(::PROTOBUF_NAMESPACE_ID::internal::GetEmptyString).size()), target);
  }
  // @@protoc_insertion_point(serialize_to_array_end:onnx.OperatorSetProto)
  return target;
}

size_t OperatorSetProto::ByteSizeLong() const {
// @@protoc_insertion_point(message_byte_size_start:onnx.OperatorSetProto)
  size_t total_size = 0;

  uint32_t cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  // repeated .onnx.OperatorProto operator = 8;
  total_size += 1UL * this->_internal_operator__size();
  for (const auto& msg : this->operator__) {
    total_size +=
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::MessageSize(msg);
  }

  // repeated .onnx.FunctionProto functions = 9;
  total_size += 1UL * this->_internal_functions_size();
  for (const auto& msg : this->functions_) {
    total_size +=
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::MessageSize(msg);
  }

  // string magic = 1;
  if (!this->_internal_magic().empty()) {
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::StringSize(
        this->_internal_magic());
  }

  // string ir_version_prerelease = 3;
  if (!this->_internal_ir_version_prerelease().empty()) {
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::StringSize(
        this->_internal_ir_version_prerelease());
  }

  // string domain = 4;
  if (!this->_internal_domain().empty()) {
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::StringSize(
        this->_internal_domain());
  }

  // string doc_string = 6;
  if (!this->_internal_doc_string().empty()) {
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::StringSize(
        this->_internal_doc_string());
  }

  // string ir_build_metadata = 7;
  if (!this->_internal_ir_build_metadata().empty()) {
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::StringSize(
        this->_internal_ir_build_metadata());
  }

  // int64 ir_version = 2;
  if (this->_internal_ir_version() != 0) {
    total_size += ::_pbi::WireFormatLite::Int64SizePlusOne(this->_internal_ir_version());
  }

  // int64 opset_version = 5;
  if (this->_internal_opset_version() != 0) {
    total_size += ::_pbi::WireFormatLite::Int64SizePlusOne(this->_internal_opset_version());
  }

  if (PROTOBUF_PREDICT_FALSE(_internal_metadata_.have_unknown_fields())) {
    total_size += _internal_metadata_.unknown_fields<std::string>(::PROTOBUF_NAMESPACE_ID::internal::GetEmptyString).size();
  }
  int cached_size = ::_pbi::ToCachedSize(total_size);
  SetCachedSize(cached_size);
  return total_size;
}

void OperatorSetProto::CheckTypeAndMergeFrom(
    const ::PROTOBUF_NAMESPACE_ID::MessageLite& from) {
  MergeFrom(*::_pbi::DownCast<const OperatorSetProto*>(
      &from));
}

void OperatorSetProto::MergeFrom(const OperatorSetProto& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:onnx.OperatorSetProto)
  GOOGLE_DCHECK_NE(&from, this);
  uint32_t cached_has_bits = 0;
  (void) cached_has_bits;

  operator__.MergeFrom(from.operator__);
  functions_.MergeFrom(from.functions_);
  if (!from._internal_magic().empty()) {
    _internal_set_magic(from._internal_magic());
  }
  if (!from._internal_ir_version_prerelease().empty()) {
    _internal_set_ir_version_prerelease(from._internal_ir_version_prerelease());
  }
  if (!from._internal_domain().empty()) {
    _internal_set_domain(from._internal_domain());
  }
  if (!from._internal_doc_string().empty()) {
    _internal_set_doc_string(from._internal_doc_string());
  }
  if (!from._internal_ir_build_metadata().empty()) {
    _internal_set_ir_build_metadata(from._internal_ir_build_metadata());
  }
  if (from._internal_ir_version() != 0) {
    _internal_set_ir_version(from._internal_ir_version());
  }
  if (from._internal_opset_version() != 0) {
    _internal_set_opset_version(from._internal_opset_version());
  }
  _internal_metadata_.MergeFrom<std::string>(from._internal_metadata_);
}

void OperatorSetProto::CopyFrom(const OperatorSetProto& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:onnx.OperatorSetProto)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool OperatorSetProto::IsInitialized() const {
  return true;
}

void OperatorSetProto::InternalSwap(OperatorSetProto* other) {
  using std::swap;
  auto* lhs_arena = GetArenaForAllocation();
  auto* rhs_arena = other->GetArenaForAllocation();
  _internal_metadata_.InternalSwap(&other->_internal_metadata_);
  operator__.InternalSwap(&other->operator__);
  functions_.InternalSwap(&other->functions_);
  ::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr::InternalSwap(
      &magic_, lhs_arena,
      &other->magic_, rhs_arena
  );
  ::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr::InternalSwap(
      &ir_version_prerelease_, lhs_arena,
      &other->ir_version_prerelease_, rhs_arena
  );
  ::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr::InternalSwap(
      &domain_, lhs_arena,
      &other->domain_, rhs_arena
  );
  ::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr::InternalSwap(
      &doc_string_, lhs_arena,
      &other->doc_string_, rhs_arena
  );
  ::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr::InternalSwap(
      &ir_build_metadata_, lhs_arena,
      &other->ir_build_metadata_, rhs_arena
  );
  ::PROTOBUF_NAMESPACE_ID::internal::memswap<
      PROTOBUF_FIELD_OFFSET(OperatorSetProto, opset_version_)
      + sizeof(OperatorSetProto::opset_version_)
      - PROTOBUF_FIELD_OFFSET(OperatorSetProto, ir_version_)>(
          reinterpret_cast<char*>(&ir_version_),
          reinterpret_cast<char*>(&other->ir_version_));
}

std::string OperatorSetProto::GetTypeName() const {
  return "onnx.OperatorSetProto";
}


// @@protoc_insertion_point(namespace_scope)
}  // namespace onnx
PROTOBUF_NAMESPACE_OPEN
template<> PROTOBUF_NOINLINE ::onnx::OperatorProto*
Arena::CreateMaybeMessage< ::onnx::OperatorProto >(Arena* arena) {
  return Arena::CreateMessageInternal< ::onnx::OperatorProto >(arena);
}
template<> PROTOBUF_NOINLINE ::onnx::OperatorSetProto*
Arena::CreateMaybeMessage< ::onnx::OperatorSetProto >(Arena* arena) {
  return Arena::CreateMessageInternal< ::onnx::OperatorSetProto >(arena);
}
PROTOBUF_NAMESPACE_CLOSE

// @@protoc_insertion_point(global_scope)
#include <google/protobuf/port_undef.inc>
