// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: object_detection/protos/mean_stddev_box_coder.proto

#include "object_detection/protos/mean_stddev_box_coder.pb.h"

#include <algorithm>

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/wire_format_lite.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/reflection_ops.h>
#include <google/protobuf/wire_format.h>
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>

PROTOBUF_PRAGMA_INIT_SEG

namespace _pb = ::PROTOBUF_NAMESPACE_ID;
namespace _pbi = _pb::internal;

namespace object_detection {
namespace protos {
PROTOBUF_CONSTEXPR MeanStddevBoxCoder::MeanStddevBoxCoder(
    ::_pbi::ConstantInitialized)
  : stddev_(0.01f){}
struct MeanStddevBoxCoderDefaultTypeInternal {
  PROTOBUF_CONSTEXPR MeanStddevBoxCoderDefaultTypeInternal()
      : _instance(::_pbi::ConstantInitialized{}) {}
  ~MeanStddevBoxCoderDefaultTypeInternal() {}
  union {
    MeanStddevBoxCoder _instance;
  };
};
PROTOBUF_ATTRIBUTE_NO_DESTROY PROTOBUF_CONSTINIT PROTOBUF_ATTRIBUTE_INIT_PRIORITY1 MeanStddevBoxCoderDefaultTypeInternal _MeanStddevBoxCoder_default_instance_;
}  // namespace protos
}  // namespace object_detection
static ::_pb::Metadata file_level_metadata_object_5fdetection_2fprotos_2fmean_5fstddev_5fbox_5fcoder_2eproto[1];
static constexpr ::_pb::EnumDescriptor const** file_level_enum_descriptors_object_5fdetection_2fprotos_2fmean_5fstddev_5fbox_5fcoder_2eproto = nullptr;
static constexpr ::_pb::ServiceDescriptor const** file_level_service_descriptors_object_5fdetection_2fprotos_2fmean_5fstddev_5fbox_5fcoder_2eproto = nullptr;

const uint32_t TableStruct_object_5fdetection_2fprotos_2fmean_5fstddev_5fbox_5fcoder_2eproto::offsets[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) = {
  PROTOBUF_FIELD_OFFSET(::object_detection::protos::MeanStddevBoxCoder, _has_bits_),
  PROTOBUF_FIELD_OFFSET(::object_detection::protos::MeanStddevBoxCoder, _internal_metadata_),
  ~0u,  // no _extensions_
  ~0u,  // no _oneof_case_
  ~0u,  // no _weak_field_map_
  ~0u,  // no _inlined_string_donated_
  PROTOBUF_FIELD_OFFSET(::object_detection::protos::MeanStddevBoxCoder, stddev_),
  0,
};
static const ::_pbi::MigrationSchema schemas[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) = {
  { 0, 7, -1, sizeof(::object_detection::protos::MeanStddevBoxCoder)},
};

static const ::_pb::Message* const file_default_instances[] = {
  &::object_detection::protos::_MeanStddevBoxCoder_default_instance_._instance,
};

const char descriptor_table_protodef_object_5fdetection_2fprotos_2fmean_5fstddev_5fbox_5fcoder_2eproto[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) =
  "\n3object_detection/protos/mean_stddev_bo"
  "x_coder.proto\022\027object_detection.protos\"*"
  "\n\022MeanStddevBoxCoder\022\024\n\006stddev\030\001 \001(\002:\0040."
  "01"
  ;
static ::_pbi::once_flag descriptor_table_object_5fdetection_2fprotos_2fmean_5fstddev_5fbox_5fcoder_2eproto_once;
const ::_pbi::DescriptorTable descriptor_table_object_5fdetection_2fprotos_2fmean_5fstddev_5fbox_5fcoder_2eproto = {
    false, false, 122, descriptor_table_protodef_object_5fdetection_2fprotos_2fmean_5fstddev_5fbox_5fcoder_2eproto,
    "object_detection/protos/mean_stddev_box_coder.proto",
    &descriptor_table_object_5fdetection_2fprotos_2fmean_5fstddev_5fbox_5fcoder_2eproto_once, nullptr, 0, 1,
    schemas, file_default_instances, TableStruct_object_5fdetection_2fprotos_2fmean_5fstddev_5fbox_5fcoder_2eproto::offsets,
    file_level_metadata_object_5fdetection_2fprotos_2fmean_5fstddev_5fbox_5fcoder_2eproto, file_level_enum_descriptors_object_5fdetection_2fprotos_2fmean_5fstddev_5fbox_5fcoder_2eproto,
    file_level_service_descriptors_object_5fdetection_2fprotos_2fmean_5fstddev_5fbox_5fcoder_2eproto,
};
PROTOBUF_ATTRIBUTE_WEAK const ::_pbi::DescriptorTable* descriptor_table_object_5fdetection_2fprotos_2fmean_5fstddev_5fbox_5fcoder_2eproto_getter() {
  return &descriptor_table_object_5fdetection_2fprotos_2fmean_5fstddev_5fbox_5fcoder_2eproto;
}

// Force running AddDescriptors() at dynamic initialization time.
PROTOBUF_ATTRIBUTE_INIT_PRIORITY2 static ::_pbi::AddDescriptorsRunner dynamic_init_dummy_object_5fdetection_2fprotos_2fmean_5fstddev_5fbox_5fcoder_2eproto(&descriptor_table_object_5fdetection_2fprotos_2fmean_5fstddev_5fbox_5fcoder_2eproto);
namespace object_detection {
namespace protos {

// ===================================================================

class MeanStddevBoxCoder::_Internal {
 public:
  using HasBits = decltype(std::declval<MeanStddevBoxCoder>()._has_bits_);
  static void set_has_stddev(HasBits* has_bits) {
    (*has_bits)[0] |= 1u;
  }
};

MeanStddevBoxCoder::MeanStddevBoxCoder(::PROTOBUF_NAMESPACE_ID::Arena* arena,
                         bool is_message_owned)
  : ::PROTOBUF_NAMESPACE_ID::Message(arena, is_message_owned) {
  SharedCtor();
  // @@protoc_insertion_point(arena_constructor:object_detection.protos.MeanStddevBoxCoder)
}
MeanStddevBoxCoder::MeanStddevBoxCoder(const MeanStddevBoxCoder& from)
  : ::PROTOBUF_NAMESPACE_ID::Message(),
      _has_bits_(from._has_bits_) {
  _internal_metadata_.MergeFrom<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(from._internal_metadata_);
  stddev_ = from.stddev_;
  // @@protoc_insertion_point(copy_constructor:object_detection.protos.MeanStddevBoxCoder)
}

inline void MeanStddevBoxCoder::SharedCtor() {
stddev_ = 0.01f;
}

MeanStddevBoxCoder::~MeanStddevBoxCoder() {
  // @@protoc_insertion_point(destructor:object_detection.protos.MeanStddevBoxCoder)
  if (auto *arena = _internal_metadata_.DeleteReturnArena<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>()) {
  (void)arena;
    return;
  }
  SharedDtor();
}

inline void MeanStddevBoxCoder::SharedDtor() {
  GOOGLE_DCHECK(GetArenaForAllocation() == nullptr);
}

void MeanStddevBoxCoder::SetCachedSize(int size) const {
  _cached_size_.Set(size);
}

void MeanStddevBoxCoder::Clear() {
// @@protoc_insertion_point(message_clear_start:object_detection.protos.MeanStddevBoxCoder)
  uint32_t cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  stddev_ = 0.01f;
  _has_bits_.Clear();
  _internal_metadata_.Clear<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>();
}

const char* MeanStddevBoxCoder::_InternalParse(const char* ptr, ::_pbi::ParseContext* ctx) {
#define CHK_(x) if (PROTOBUF_PREDICT_FALSE(!(x))) goto failure
  _Internal::HasBits has_bits{};
  while (!ctx->Done(&ptr)) {
    uint32_t tag;
    ptr = ::_pbi::ReadTag(ptr, &tag);
    switch (tag >> 3) {
      // optional float stddev = 1 [default = 0.01];
      case 1:
        if (PROTOBUF_PREDICT_TRUE(static_cast<uint8_t>(tag) == 13)) {
          _Internal::set_has_stddev(&has_bits);
          stddev_ = ::PROTOBUF_NAMESPACE_ID::internal::UnalignedLoad<float>(ptr);
          ptr += sizeof(float);
        } else
          goto handle_unusual;
        continue;
      default:
        goto handle_unusual;
    }  // switch
  handle_unusual:
    if ((tag == 0) || ((tag & 7) == 4)) {
      CHK_(ptr);
      ctx->SetLastTag(tag);
      goto message_done;
    }
    ptr = UnknownFieldParse(
        tag,
        _internal_metadata_.mutable_unknown_fields<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(),
        ptr, ctx);
    CHK_(ptr != nullptr);
  }  // while
message_done:
  _has_bits_.Or(has_bits);
  return ptr;
failure:
  ptr = nullptr;
  goto message_done;
#undef CHK_
}

uint8_t* MeanStddevBoxCoder::_InternalSerialize(
    uint8_t* target, ::PROTOBUF_NAMESPACE_ID::io::EpsCopyOutputStream* stream) const {
  // @@protoc_insertion_point(serialize_to_array_start:object_detection.protos.MeanStddevBoxCoder)
  uint32_t cached_has_bits = 0;
  (void) cached_has_bits;

  cached_has_bits = _has_bits_[0];
  // optional float stddev = 1 [default = 0.01];
  if (cached_has_bits & 0x00000001u) {
    target = stream->EnsureSpace(target);
    target = ::_pbi::WireFormatLite::WriteFloatToArray(1, this->_internal_stddev(), target);
  }

  if (PROTOBUF_PREDICT_FALSE(_internal_metadata_.have_unknown_fields())) {
    target = ::_pbi::WireFormat::InternalSerializeUnknownFieldsToArray(
        _internal_metadata_.unknown_fields<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(::PROTOBUF_NAMESPACE_ID::UnknownFieldSet::default_instance), target, stream);
  }
  // @@protoc_insertion_point(serialize_to_array_end:object_detection.protos.MeanStddevBoxCoder)
  return target;
}

size_t MeanStddevBoxCoder::ByteSizeLong() const {
// @@protoc_insertion_point(message_byte_size_start:object_detection.protos.MeanStddevBoxCoder)
  size_t total_size = 0;

  uint32_t cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  // optional float stddev = 1 [default = 0.01];
  cached_has_bits = _has_bits_[0];
  if (cached_has_bits & 0x00000001u) {
    total_size += 1 + 4;
  }

  return MaybeComputeUnknownFieldsSize(total_size, &_cached_size_);
}

const ::PROTOBUF_NAMESPACE_ID::Message::ClassData MeanStddevBoxCoder::_class_data_ = {
    ::PROTOBUF_NAMESPACE_ID::Message::CopyWithSizeCheck,
    MeanStddevBoxCoder::MergeImpl
};
const ::PROTOBUF_NAMESPACE_ID::Message::ClassData*MeanStddevBoxCoder::GetClassData() const { return &_class_data_; }

void MeanStddevBoxCoder::MergeImpl(::PROTOBUF_NAMESPACE_ID::Message* to,
                      const ::PROTOBUF_NAMESPACE_ID::Message& from) {
  static_cast<MeanStddevBoxCoder *>(to)->MergeFrom(
      static_cast<const MeanStddevBoxCoder &>(from));
}


void MeanStddevBoxCoder::MergeFrom(const MeanStddevBoxCoder& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:object_detection.protos.MeanStddevBoxCoder)
  GOOGLE_DCHECK_NE(&from, this);
  uint32_t cached_has_bits = 0;
  (void) cached_has_bits;

  if (from._internal_has_stddev()) {
    _internal_set_stddev(from._internal_stddev());
  }
  _internal_metadata_.MergeFrom<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(from._internal_metadata_);
}

void MeanStddevBoxCoder::CopyFrom(const MeanStddevBoxCoder& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:object_detection.protos.MeanStddevBoxCoder)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool MeanStddevBoxCoder::IsInitialized() const {
  return true;
}

void MeanStddevBoxCoder::InternalSwap(MeanStddevBoxCoder* other) {
  using std::swap;
  _internal_metadata_.InternalSwap(&other->_internal_metadata_);
  swap(_has_bits_[0], other->_has_bits_[0]);
  swap(stddev_, other->stddev_);
}

::PROTOBUF_NAMESPACE_ID::Metadata MeanStddevBoxCoder::GetMetadata() const {
  return ::_pbi::AssignDescriptors(
      &descriptor_table_object_5fdetection_2fprotos_2fmean_5fstddev_5fbox_5fcoder_2eproto_getter, &descriptor_table_object_5fdetection_2fprotos_2fmean_5fstddev_5fbox_5fcoder_2eproto_once,
      file_level_metadata_object_5fdetection_2fprotos_2fmean_5fstddev_5fbox_5fcoder_2eproto[0]);
}

// @@protoc_insertion_point(namespace_scope)
}  // namespace protos
}  // namespace object_detection
PROTOBUF_NAMESPACE_OPEN
template<> PROTOBUF_NOINLINE ::object_detection::protos::MeanStddevBoxCoder*
Arena::CreateMaybeMessage< ::object_detection::protos::MeanStddevBoxCoder >(Arena* arena) {
  return Arena::CreateMessageInternal< ::object_detection::protos::MeanStddevBoxCoder >(arena);
}
PROTOBUF_NAMESPACE_CLOSE

// @@protoc_insertion_point(global_scope)
#include <google/protobuf/port_undef.inc>
