// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: object_detection/protos/grid_anchor_generator.proto

#ifndef GOOGLE_PROTOBUF_INCLUDED_object_5fdetection_2fprotos_2fgrid_5fanchor_5fgenerator_2eproto
#define GOOGLE_PROTOBUF_INCLUDED_object_5fdetection_2fprotos_2fgrid_5fanchor_5fgenerator_2eproto

#include <limits>
#include <string>

#include <google/protobuf/port_def.inc>
#if PROTOBUF_VERSION < 3020000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers. Please update
#error your headers.
#endif
#if 3020002 < PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers. Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/port_undef.inc>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/metadata_lite.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>  // IWYU pragma: export
#include <google/protobuf/extension_set.h>  // IWYU pragma: export
#include <google/protobuf/unknown_field_set.h>
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>
#define PROTOBUF_INTERNAL_EXPORT_object_5fdetection_2fprotos_2fgrid_5fanchor_5fgenerator_2eproto
PROTOBUF_NAMESPACE_OPEN
namespace internal {
class AnyMetadata;
}  // namespace internal
PROTOBUF_NAMESPACE_CLOSE

// Internal implementation detail -- do not use these members.
struct TableStruct_object_5fdetection_2fprotos_2fgrid_5fanchor_5fgenerator_2eproto {
  static const uint32_t offsets[];
};
extern const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable descriptor_table_object_5fdetection_2fprotos_2fgrid_5fanchor_5fgenerator_2eproto;
namespace object_detection {
namespace protos {
class GridAnchorGenerator;
struct GridAnchorGeneratorDefaultTypeInternal;
extern GridAnchorGeneratorDefaultTypeInternal _GridAnchorGenerator_default_instance_;
}  // namespace protos
}  // namespace object_detection
PROTOBUF_NAMESPACE_OPEN
template<> ::object_detection::protos::GridAnchorGenerator* Arena::CreateMaybeMessage<::object_detection::protos::GridAnchorGenerator>(Arena*);
PROTOBUF_NAMESPACE_CLOSE
namespace object_detection {
namespace protos {

// ===================================================================

class GridAnchorGenerator final :
    public ::PROTOBUF_NAMESPACE_ID::Message /* @@protoc_insertion_point(class_definition:object_detection.protos.GridAnchorGenerator) */ {
 public:
  inline GridAnchorGenerator() : GridAnchorGenerator(nullptr) {}
  ~GridAnchorGenerator() override;
  explicit PROTOBUF_CONSTEXPR GridAnchorGenerator(::PROTOBUF_NAMESPACE_ID::internal::ConstantInitialized);

  GridAnchorGenerator(const GridAnchorGenerator& from);
  GridAnchorGenerator(GridAnchorGenerator&& from) noexcept
    : GridAnchorGenerator() {
    *this = ::std::move(from);
  }

  inline GridAnchorGenerator& operator=(const GridAnchorGenerator& from) {
    CopyFrom(from);
    return *this;
  }
  inline GridAnchorGenerator& operator=(GridAnchorGenerator&& from) noexcept {
    if (this == &from) return *this;
    if (GetOwningArena() == from.GetOwningArena()
  #ifdef PROTOBUF_FORCE_COPY_IN_MOVE
        && GetOwningArena() != nullptr
  #endif  // !PROTOBUF_FORCE_COPY_IN_MOVE
    ) {
      InternalSwap(&from);
    } else {
      CopyFrom(from);
    }
    return *this;
  }

  inline const ::PROTOBUF_NAMESPACE_ID::UnknownFieldSet& unknown_fields() const {
    return _internal_metadata_.unknown_fields<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(::PROTOBUF_NAMESPACE_ID::UnknownFieldSet::default_instance);
  }
  inline ::PROTOBUF_NAMESPACE_ID::UnknownFieldSet* mutable_unknown_fields() {
    return _internal_metadata_.mutable_unknown_fields<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>();
  }

  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* descriptor() {
    return GetDescriptor();
  }
  static const ::PROTOBUF_NAMESPACE_ID::Descriptor* GetDescriptor() {
    return default_instance().GetMetadata().descriptor;
  }
  static const ::PROTOBUF_NAMESPACE_ID::Reflection* GetReflection() {
    return default_instance().GetMetadata().reflection;
  }
  static const GridAnchorGenerator& default_instance() {
    return *internal_default_instance();
  }
  static inline const GridAnchorGenerator* internal_default_instance() {
    return reinterpret_cast<const GridAnchorGenerator*>(
               &_GridAnchorGenerator_default_instance_);
  }
  static constexpr int kIndexInFileMessages =
    0;

  friend void swap(GridAnchorGenerator& a, GridAnchorGenerator& b) {
    a.Swap(&b);
  }
  inline void Swap(GridAnchorGenerator* other) {
    if (other == this) return;
  #ifdef PROTOBUF_FORCE_COPY_IN_SWAP
    if (GetOwningArena() != nullptr &&
        GetOwningArena() == other->GetOwningArena()) {
   #else  // PROTOBUF_FORCE_COPY_IN_SWAP
    if (GetOwningArena() == other->GetOwningArena()) {
  #endif  // !PROTOBUF_FORCE_COPY_IN_SWAP
      InternalSwap(other);
    } else {
      ::PROTOBUF_NAMESPACE_ID::internal::GenericSwap(this, other);
    }
  }
  void UnsafeArenaSwap(GridAnchorGenerator* other) {
    if (other == this) return;
    GOOGLE_DCHECK(GetOwningArena() == other->GetOwningArena());
    InternalSwap(other);
  }

  // implements Message ----------------------------------------------

  GridAnchorGenerator* New(::PROTOBUF_NAMESPACE_ID::Arena* arena = nullptr) const final {
    return CreateMaybeMessage<GridAnchorGenerator>(arena);
  }
  using ::PROTOBUF_NAMESPACE_ID::Message::CopyFrom;
  void CopyFrom(const GridAnchorGenerator& from);
  using ::PROTOBUF_NAMESPACE_ID::Message::MergeFrom;
  void MergeFrom(const GridAnchorGenerator& from);
  private:
  static void MergeImpl(::PROTOBUF_NAMESPACE_ID::Message* to, const ::PROTOBUF_NAMESPACE_ID::Message& from);
  public:
  PROTOBUF_ATTRIBUTE_REINITIALIZES void Clear() final;
  bool IsInitialized() const final;

  size_t ByteSizeLong() const final;
  const char* _InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) final;
  uint8_t* _InternalSerialize(
      uint8_t* target, ::PROTOBUF_NAMESPACE_ID::io::EpsCopyOutputStream* stream) const final;
  int GetCachedSize() const final { return _cached_size_.Get(); }

  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const final;
  void InternalSwap(GridAnchorGenerator* other);

  private:
  friend class ::PROTOBUF_NAMESPACE_ID::internal::AnyMetadata;
  static ::PROTOBUF_NAMESPACE_ID::StringPiece FullMessageName() {
    return "object_detection.protos.GridAnchorGenerator";
  }
  protected:
  explicit GridAnchorGenerator(::PROTOBUF_NAMESPACE_ID::Arena* arena,
                       bool is_message_owned = false);
  public:

  static const ClassData _class_data_;
  const ::PROTOBUF_NAMESPACE_ID::Message::ClassData*GetClassData() const final;

  ::PROTOBUF_NAMESPACE_ID::Metadata GetMetadata() const final;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  enum : int {
    kScalesFieldNumber = 7,
    kAspectRatiosFieldNumber = 8,
    kHeightOffsetFieldNumber = 5,
    kWidthOffsetFieldNumber = 6,
    kHeightFieldNumber = 1,
    kWidthFieldNumber = 2,
    kHeightStrideFieldNumber = 3,
    kWidthStrideFieldNumber = 4,
  };
  // repeated float scales = 7;
  int scales_size() const;
  private:
  int _internal_scales_size() const;
  public:
  void clear_scales();
  private:
  float _internal_scales(int index) const;
  const ::PROTOBUF_NAMESPACE_ID::RepeatedField< float >&
      _internal_scales() const;
  void _internal_add_scales(float value);
  ::PROTOBUF_NAMESPACE_ID::RepeatedField< float >*
      _internal_mutable_scales();
  public:
  float scales(int index) const;
  void set_scales(int index, float value);
  void add_scales(float value);
  const ::PROTOBUF_NAMESPACE_ID::RepeatedField< float >&
      scales() const;
  ::PROTOBUF_NAMESPACE_ID::RepeatedField< float >*
      mutable_scales();

  // repeated float aspect_ratios = 8;
  int aspect_ratios_size() const;
  private:
  int _internal_aspect_ratios_size() const;
  public:
  void clear_aspect_ratios();
  private:
  float _internal_aspect_ratios(int index) const;
  const ::PROTOBUF_NAMESPACE_ID::RepeatedField< float >&
      _internal_aspect_ratios() const;
  void _internal_add_aspect_ratios(float value);
  ::PROTOBUF_NAMESPACE_ID::RepeatedField< float >*
      _internal_mutable_aspect_ratios();
  public:
  float aspect_ratios(int index) const;
  void set_aspect_ratios(int index, float value);
  void add_aspect_ratios(float value);
  const ::PROTOBUF_NAMESPACE_ID::RepeatedField< float >&
      aspect_ratios() const;
  ::PROTOBUF_NAMESPACE_ID::RepeatedField< float >*
      mutable_aspect_ratios();

  // optional int32 height_offset = 5 [default = 0];
  bool has_height_offset() const;
  private:
  bool _internal_has_height_offset() const;
  public:
  void clear_height_offset();
  int32_t height_offset() const;
  void set_height_offset(int32_t value);
  private:
  int32_t _internal_height_offset() const;
  void _internal_set_height_offset(int32_t value);
  public:

  // optional int32 width_offset = 6 [default = 0];
  bool has_width_offset() const;
  private:
  bool _internal_has_width_offset() const;
  public:
  void clear_width_offset();
  int32_t width_offset() const;
  void set_width_offset(int32_t value);
  private:
  int32_t _internal_width_offset() const;
  void _internal_set_width_offset(int32_t value);
  public:

  // optional int32 height = 1 [default = 256];
  bool has_height() const;
  private:
  bool _internal_has_height() const;
  public:
  void clear_height();
  int32_t height() const;
  void set_height(int32_t value);
  private:
  int32_t _internal_height() const;
  void _internal_set_height(int32_t value);
  public:

  // optional int32 width = 2 [default = 256];
  bool has_width() const;
  private:
  bool _internal_has_width() const;
  public:
  void clear_width();
  int32_t width() const;
  void set_width(int32_t value);
  private:
  int32_t _internal_width() const;
  void _internal_set_width(int32_t value);
  public:

  // optional int32 height_stride = 3 [default = 16];
  bool has_height_stride() const;
  private:
  bool _internal_has_height_stride() const;
  public:
  void clear_height_stride();
  int32_t height_stride() const;
  void set_height_stride(int32_t value);
  private:
  int32_t _internal_height_stride() const;
  void _internal_set_height_stride(int32_t value);
  public:

  // optional int32 width_stride = 4 [default = 16];
  bool has_width_stride() const;
  private:
  bool _internal_has_width_stride() const;
  public:
  void clear_width_stride();
  int32_t width_stride() const;
  void set_width_stride(int32_t value);
  private:
  int32_t _internal_width_stride() const;
  void _internal_set_width_stride(int32_t value);
  public:

  // @@protoc_insertion_point(class_scope:object_detection.protos.GridAnchorGenerator)
 private:
  class _Internal;

  template <typename T> friend class ::PROTOBUF_NAMESPACE_ID::Arena::InternalHelper;
  typedef void InternalArenaConstructable_;
  typedef void DestructorSkippable_;
  ::PROTOBUF_NAMESPACE_ID::internal::HasBits<1> _has_bits_;
  mutable ::PROTOBUF_NAMESPACE_ID::internal::CachedSize _cached_size_;
  ::PROTOBUF_NAMESPACE_ID::RepeatedField< float > scales_;
  ::PROTOBUF_NAMESPACE_ID::RepeatedField< float > aspect_ratios_;
  int32_t height_offset_;
  int32_t width_offset_;
  int32_t height_;
  int32_t width_;
  int32_t height_stride_;
  int32_t width_stride_;
  friend struct ::TableStruct_object_5fdetection_2fprotos_2fgrid_5fanchor_5fgenerator_2eproto;
};
// ===================================================================


// ===================================================================

#ifdef __GNUC__
  #pragma GCC diagnostic push
  #pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif  // __GNUC__
// GridAnchorGenerator

// optional int32 height = 1 [default = 256];
inline bool GridAnchorGenerator::_internal_has_height() const {
  bool value = (_has_bits_[0] & 0x00000004u) != 0;
  return value;
}
inline bool GridAnchorGenerator::has_height() const {
  return _internal_has_height();
}
inline void GridAnchorGenerator::clear_height() {
  height_ = 256;
  _has_bits_[0] &= ~0x00000004u;
}
inline int32_t GridAnchorGenerator::_internal_height() const {
  return height_;
}
inline int32_t GridAnchorGenerator::height() const {
  // @@protoc_insertion_point(field_get:object_detection.protos.GridAnchorGenerator.height)
  return _internal_height();
}
inline void GridAnchorGenerator::_internal_set_height(int32_t value) {
  _has_bits_[0] |= 0x00000004u;
  height_ = value;
}
inline void GridAnchorGenerator::set_height(int32_t value) {
  _internal_set_height(value);
  // @@protoc_insertion_point(field_set:object_detection.protos.GridAnchorGenerator.height)
}

// optional int32 width = 2 [default = 256];
inline bool GridAnchorGenerator::_internal_has_width() const {
  bool value = (_has_bits_[0] & 0x00000008u) != 0;
  return value;
}
inline bool GridAnchorGenerator::has_width() const {
  return _internal_has_width();
}
inline void GridAnchorGenerator::clear_width() {
  width_ = 256;
  _has_bits_[0] &= ~0x00000008u;
}
inline int32_t GridAnchorGenerator::_internal_width() const {
  return width_;
}
inline int32_t GridAnchorGenerator::width() const {
  // @@protoc_insertion_point(field_get:object_detection.protos.GridAnchorGenerator.width)
  return _internal_width();
}
inline void GridAnchorGenerator::_internal_set_width(int32_t value) {
  _has_bits_[0] |= 0x00000008u;
  width_ = value;
}
inline void GridAnchorGenerator::set_width(int32_t value) {
  _internal_set_width(value);
  // @@protoc_insertion_point(field_set:object_detection.protos.GridAnchorGenerator.width)
}

// optional int32 height_stride = 3 [default = 16];
inline bool GridAnchorGenerator::_internal_has_height_stride() const {
  bool value = (_has_bits_[0] & 0x00000010u) != 0;
  return value;
}
inline bool GridAnchorGenerator::has_height_stride() const {
  return _internal_has_height_stride();
}
inline void GridAnchorGenerator::clear_height_stride() {
  height_stride_ = 16;
  _has_bits_[0] &= ~0x00000010u;
}
inline int32_t GridAnchorGenerator::_internal_height_stride() const {
  return height_stride_;
}
inline int32_t GridAnchorGenerator::height_stride() const {
  // @@protoc_insertion_point(field_get:object_detection.protos.GridAnchorGenerator.height_stride)
  return _internal_height_stride();
}
inline void GridAnchorGenerator::_internal_set_height_stride(int32_t value) {
  _has_bits_[0] |= 0x00000010u;
  height_stride_ = value;
}
inline void GridAnchorGenerator::set_height_stride(int32_t value) {
  _internal_set_height_stride(value);
  // @@protoc_insertion_point(field_set:object_detection.protos.GridAnchorGenerator.height_stride)
}

// optional int32 width_stride = 4 [default = 16];
inline bool GridAnchorGenerator::_internal_has_width_stride() const {
  bool value = (_has_bits_[0] & 0x00000020u) != 0;
  return value;
}
inline bool GridAnchorGenerator::has_width_stride() const {
  return _internal_has_width_stride();
}
inline void GridAnchorGenerator::clear_width_stride() {
  width_stride_ = 16;
  _has_bits_[0] &= ~0x00000020u;
}
inline int32_t GridAnchorGenerator::_internal_width_stride() const {
  return width_stride_;
}
inline int32_t GridAnchorGenerator::width_stride() const {
  // @@protoc_insertion_point(field_get:object_detection.protos.GridAnchorGenerator.width_stride)
  return _internal_width_stride();
}
inline void GridAnchorGenerator::_internal_set_width_stride(int32_t value) {
  _has_bits_[0] |= 0x00000020u;
  width_stride_ = value;
}
inline void GridAnchorGenerator::set_width_stride(int32_t value) {
  _internal_set_width_stride(value);
  // @@protoc_insertion_point(field_set:object_detection.protos.GridAnchorGenerator.width_stride)
}

// optional int32 height_offset = 5 [default = 0];
inline bool GridAnchorGenerator::_internal_has_height_offset() const {
  bool value = (_has_bits_[0] & 0x00000001u) != 0;
  return value;
}
inline bool GridAnchorGenerator::has_height_offset() const {
  return _internal_has_height_offset();
}
inline void GridAnchorGenerator::clear_height_offset() {
  height_offset_ = 0;
  _has_bits_[0] &= ~0x00000001u;
}
inline int32_t GridAnchorGenerator::_internal_height_offset() const {
  return height_offset_;
}
inline int32_t GridAnchorGenerator::height_offset() const {
  // @@protoc_insertion_point(field_get:object_detection.protos.GridAnchorGenerator.height_offset)
  return _internal_height_offset();
}
inline void GridAnchorGenerator::_internal_set_height_offset(int32_t value) {
  _has_bits_[0] |= 0x00000001u;
  height_offset_ = value;
}
inline void GridAnchorGenerator::set_height_offset(int32_t value) {
  _internal_set_height_offset(value);
  // @@protoc_insertion_point(field_set:object_detection.protos.GridAnchorGenerator.height_offset)
}

// optional int32 width_offset = 6 [default = 0];
inline bool GridAnchorGenerator::_internal_has_width_offset() const {
  bool value = (_has_bits_[0] & 0x00000002u) != 0;
  return value;
}
inline bool GridAnchorGenerator::has_width_offset() const {
  return _internal_has_width_offset();
}
inline void GridAnchorGenerator::clear_width_offset() {
  width_offset_ = 0;
  _has_bits_[0] &= ~0x00000002u;
}
inline int32_t GridAnchorGenerator::_internal_width_offset() const {
  return width_offset_;
}
inline int32_t GridAnchorGenerator::width_offset() const {
  // @@protoc_insertion_point(field_get:object_detection.protos.GridAnchorGenerator.width_offset)
  return _internal_width_offset();
}
inline void GridAnchorGenerator::_internal_set_width_offset(int32_t value) {
  _has_bits_[0] |= 0x00000002u;
  width_offset_ = value;
}
inline void GridAnchorGenerator::set_width_offset(int32_t value) {
  _internal_set_width_offset(value);
  // @@protoc_insertion_point(field_set:object_detection.protos.GridAnchorGenerator.width_offset)
}

// repeated float scales = 7;
inline int GridAnchorGenerator::_internal_scales_size() const {
  return scales_.size();
}
inline int GridAnchorGenerator::scales_size() const {
  return _internal_scales_size();
}
inline void GridAnchorGenerator::clear_scales() {
  scales_.Clear();
}
inline float GridAnchorGenerator::_internal_scales(int index) const {
  return scales_.Get(index);
}
inline float GridAnchorGenerator::scales(int index) const {
  // @@protoc_insertion_point(field_get:object_detection.protos.GridAnchorGenerator.scales)
  return _internal_scales(index);
}
inline void GridAnchorGenerator::set_scales(int index, float value) {
  scales_.Set(index, value);
  // @@protoc_insertion_point(field_set:object_detection.protos.GridAnchorGenerator.scales)
}
inline void GridAnchorGenerator::_internal_add_scales(float value) {
  scales_.Add(value);
}
inline void GridAnchorGenerator::add_scales(float value) {
  _internal_add_scales(value);
  // @@protoc_insertion_point(field_add:object_detection.protos.GridAnchorGenerator.scales)
}
inline const ::PROTOBUF_NAMESPACE_ID::RepeatedField< float >&
GridAnchorGenerator::_internal_scales() const {
  return scales_;
}
inline const ::PROTOBUF_NAMESPACE_ID::RepeatedField< float >&
GridAnchorGenerator::scales() const {
  // @@protoc_insertion_point(field_list:object_detection.protos.GridAnchorGenerator.scales)
  return _internal_scales();
}
inline ::PROTOBUF_NAMESPACE_ID::RepeatedField< float >*
GridAnchorGenerator::_internal_mutable_scales() {
  return &scales_;
}
inline ::PROTOBUF_NAMESPACE_ID::RepeatedField< float >*
GridAnchorGenerator::mutable_scales() {
  // @@protoc_insertion_point(field_mutable_list:object_detection.protos.GridAnchorGenerator.scales)
  return _internal_mutable_scales();
}

// repeated float aspect_ratios = 8;
inline int GridAnchorGenerator::_internal_aspect_ratios_size() const {
  return aspect_ratios_.size();
}
inline int GridAnchorGenerator::aspect_ratios_size() const {
  return _internal_aspect_ratios_size();
}
inline void GridAnchorGenerator::clear_aspect_ratios() {
  aspect_ratios_.Clear();
}
inline float GridAnchorGenerator::_internal_aspect_ratios(int index) const {
  return aspect_ratios_.Get(index);
}
inline float GridAnchorGenerator::aspect_ratios(int index) const {
  // @@protoc_insertion_point(field_get:object_detection.protos.GridAnchorGenerator.aspect_ratios)
  return _internal_aspect_ratios(index);
}
inline void GridAnchorGenerator::set_aspect_ratios(int index, float value) {
  aspect_ratios_.Set(index, value);
  // @@protoc_insertion_point(field_set:object_detection.protos.GridAnchorGenerator.aspect_ratios)
}
inline void GridAnchorGenerator::_internal_add_aspect_ratios(float value) {
  aspect_ratios_.Add(value);
}
inline void GridAnchorGenerator::add_aspect_ratios(float value) {
  _internal_add_aspect_ratios(value);
  // @@protoc_insertion_point(field_add:object_detection.protos.GridAnchorGenerator.aspect_ratios)
}
inline const ::PROTOBUF_NAMESPACE_ID::RepeatedField< float >&
GridAnchorGenerator::_internal_aspect_ratios() const {
  return aspect_ratios_;
}
inline const ::PROTOBUF_NAMESPACE_ID::RepeatedField< float >&
GridAnchorGenerator::aspect_ratios() const {
  // @@protoc_insertion_point(field_list:object_detection.protos.GridAnchorGenerator.aspect_ratios)
  return _internal_aspect_ratios();
}
inline ::PROTOBUF_NAMESPACE_ID::RepeatedField< float >*
GridAnchorGenerator::_internal_mutable_aspect_ratios() {
  return &aspect_ratios_;
}
inline ::PROTOBUF_NAMESPACE_ID::RepeatedField< float >*
GridAnchorGenerator::mutable_aspect_ratios() {
  // @@protoc_insertion_point(field_mutable_list:object_detection.protos.GridAnchorGenerator.aspect_ratios)
  return _internal_mutable_aspect_ratios();
}

#ifdef __GNUC__
  #pragma GCC diagnostic pop
#endif  // __GNUC__

// @@protoc_insertion_point(namespace_scope)

}  // namespace protos
}  // namespace object_detection

// @@protoc_insertion_point(global_scope)

#include <google/protobuf/port_undef.inc>
#endif  // GOOGLE_PROTOBUF_INCLUDED_GOOGLE_PROTOBUF_INCLUDED_object_5fdetection_2fprotos_2fgrid_5fanchor_5fgenerator_2eproto
