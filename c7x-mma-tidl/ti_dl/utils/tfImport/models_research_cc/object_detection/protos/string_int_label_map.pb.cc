// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: object_detection/protos/string_int_label_map.proto

#include "object_detection/protos/string_int_label_map.pb.h"

#include <algorithm>

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/wire_format_lite.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/reflection_ops.h>
#include <google/protobuf/wire_format.h>
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>

PROTOBUF_PRAGMA_INIT_SEG

namespace _pb = ::PROTOBUF_NAMESPACE_ID;
namespace _pbi = _pb::internal;

namespace object_detection {
namespace protos {
PROTOBUF_CONSTEXPR StringIntLabelMapItem::StringIntLabelMapItem(
    ::_pbi::ConstantInitialized)
  : name_(&::_pbi::fixed_address_empty_string, ::_pbi::ConstantInitialized{})
  , display_name_(&::_pbi::fixed_address_empty_string, ::_pbi::ConstantInitialized{})
  , id_(0){}
struct StringIntLabelMapItemDefaultTypeInternal {
  PROTOBUF_CONSTEXPR StringIntLabelMapItemDefaultTypeInternal()
      : _instance(::_pbi::ConstantInitialized{}) {}
  ~StringIntLabelMapItemDefaultTypeInternal() {}
  union {
    StringIntLabelMapItem _instance;
  };
};
PROTOBUF_ATTRIBUTE_NO_DESTROY PROTOBUF_CONSTINIT PROTOBUF_ATTRIBUTE_INIT_PRIORITY1 StringIntLabelMapItemDefaultTypeInternal _StringIntLabelMapItem_default_instance_;
PROTOBUF_CONSTEXPR StringIntLabelMap::StringIntLabelMap(
    ::_pbi::ConstantInitialized)
  : item_(){}
struct StringIntLabelMapDefaultTypeInternal {
  PROTOBUF_CONSTEXPR StringIntLabelMapDefaultTypeInternal()
      : _instance(::_pbi::ConstantInitialized{}) {}
  ~StringIntLabelMapDefaultTypeInternal() {}
  union {
    StringIntLabelMap _instance;
  };
};
PROTOBUF_ATTRIBUTE_NO_DESTROY PROTOBUF_CONSTINIT PROTOBUF_ATTRIBUTE_INIT_PRIORITY1 StringIntLabelMapDefaultTypeInternal _StringIntLabelMap_default_instance_;
}  // namespace protos
}  // namespace object_detection
static ::_pb::Metadata file_level_metadata_object_5fdetection_2fprotos_2fstring_5fint_5flabel_5fmap_2eproto[2];
static constexpr ::_pb::EnumDescriptor const** file_level_enum_descriptors_object_5fdetection_2fprotos_2fstring_5fint_5flabel_5fmap_2eproto = nullptr;
static constexpr ::_pb::ServiceDescriptor const** file_level_service_descriptors_object_5fdetection_2fprotos_2fstring_5fint_5flabel_5fmap_2eproto = nullptr;

const uint32_t TableStruct_object_5fdetection_2fprotos_2fstring_5fint_5flabel_5fmap_2eproto::offsets[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) = {
  PROTOBUF_FIELD_OFFSET(::object_detection::protos::StringIntLabelMapItem, _has_bits_),
  PROTOBUF_FIELD_OFFSET(::object_detection::protos::StringIntLabelMapItem, _internal_metadata_),
  ~0u,  // no _extensions_
  ~0u,  // no _oneof_case_
  ~0u,  // no _weak_field_map_
  ~0u,  // no _inlined_string_donated_
  PROTOBUF_FIELD_OFFSET(::object_detection::protos::StringIntLabelMapItem, name_),
  PROTOBUF_FIELD_OFFSET(::object_detection::protos::StringIntLabelMapItem, id_),
  PROTOBUF_FIELD_OFFSET(::object_detection::protos::StringIntLabelMapItem, display_name_),
  0,
  2,
  1,
  ~0u,  // no _has_bits_
  PROTOBUF_FIELD_OFFSET(::object_detection::protos::StringIntLabelMap, _internal_metadata_),
  ~0u,  // no _extensions_
  ~0u,  // no _oneof_case_
  ~0u,  // no _weak_field_map_
  ~0u,  // no _inlined_string_donated_
  PROTOBUF_FIELD_OFFSET(::object_detection::protos::StringIntLabelMap, item_),
};
static const ::_pbi::MigrationSchema schemas[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) = {
  { 0, 9, -1, sizeof(::object_detection::protos::StringIntLabelMapItem)},
  { 12, -1, -1, sizeof(::object_detection::protos::StringIntLabelMap)},
};

static const ::_pb::Message* const file_default_instances[] = {
  &::object_detection::protos::_StringIntLabelMapItem_default_instance_._instance,
  &::object_detection::protos::_StringIntLabelMap_default_instance_._instance,
};

const char descriptor_table_protodef_object_5fdetection_2fprotos_2fstring_5fint_5flabel_5fmap_2eproto[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) =
  "\n2object_detection/protos/string_int_lab"
  "el_map.proto\022\027object_detection.protos\"G\n"
  "\025StringIntLabelMapItem\022\014\n\004name\030\001 \001(\t\022\n\n\002"
  "id\030\002 \001(\005\022\024\n\014display_name\030\003 \001(\t\"Q\n\021String"
  "IntLabelMap\022<\n\004item\030\001 \003(\0132..object_detec"
  "tion.protos.StringIntLabelMapItem"
  ;
static ::_pbi::once_flag descriptor_table_object_5fdetection_2fprotos_2fstring_5fint_5flabel_5fmap_2eproto_once;
const ::_pbi::DescriptorTable descriptor_table_object_5fdetection_2fprotos_2fstring_5fint_5flabel_5fmap_2eproto = {
    false, false, 233, descriptor_table_protodef_object_5fdetection_2fprotos_2fstring_5fint_5flabel_5fmap_2eproto,
    "object_detection/protos/string_int_label_map.proto",
    &descriptor_table_object_5fdetection_2fprotos_2fstring_5fint_5flabel_5fmap_2eproto_once, nullptr, 0, 2,
    schemas, file_default_instances, TableStruct_object_5fdetection_2fprotos_2fstring_5fint_5flabel_5fmap_2eproto::offsets,
    file_level_metadata_object_5fdetection_2fprotos_2fstring_5fint_5flabel_5fmap_2eproto, file_level_enum_descriptors_object_5fdetection_2fprotos_2fstring_5fint_5flabel_5fmap_2eproto,
    file_level_service_descriptors_object_5fdetection_2fprotos_2fstring_5fint_5flabel_5fmap_2eproto,
};
PROTOBUF_ATTRIBUTE_WEAK const ::_pbi::DescriptorTable* descriptor_table_object_5fdetection_2fprotos_2fstring_5fint_5flabel_5fmap_2eproto_getter() {
  return &descriptor_table_object_5fdetection_2fprotos_2fstring_5fint_5flabel_5fmap_2eproto;
}

// Force running AddDescriptors() at dynamic initialization time.
PROTOBUF_ATTRIBUTE_INIT_PRIORITY2 static ::_pbi::AddDescriptorsRunner dynamic_init_dummy_object_5fdetection_2fprotos_2fstring_5fint_5flabel_5fmap_2eproto(&descriptor_table_object_5fdetection_2fprotos_2fstring_5fint_5flabel_5fmap_2eproto);
namespace object_detection {
namespace protos {

// ===================================================================

class StringIntLabelMapItem::_Internal {
 public:
  using HasBits = decltype(std::declval<StringIntLabelMapItem>()._has_bits_);
  static void set_has_name(HasBits* has_bits) {
    (*has_bits)[0] |= 1u;
  }
  static void set_has_id(HasBits* has_bits) {
    (*has_bits)[0] |= 4u;
  }
  static void set_has_display_name(HasBits* has_bits) {
    (*has_bits)[0] |= 2u;
  }
};

StringIntLabelMapItem::StringIntLabelMapItem(::PROTOBUF_NAMESPACE_ID::Arena* arena,
                         bool is_message_owned)
  : ::PROTOBUF_NAMESPACE_ID::Message(arena, is_message_owned) {
  SharedCtor();
  // @@protoc_insertion_point(arena_constructor:object_detection.protos.StringIntLabelMapItem)
}
StringIntLabelMapItem::StringIntLabelMapItem(const StringIntLabelMapItem& from)
  : ::PROTOBUF_NAMESPACE_ID::Message(),
      _has_bits_(from._has_bits_) {
  _internal_metadata_.MergeFrom<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(from._internal_metadata_);
  name_.InitDefault();
  #ifdef PROTOBUF_FORCE_COPY_DEFAULT_STRING
    name_.Set("", GetArenaForAllocation());
  #endif // PROTOBUF_FORCE_COPY_DEFAULT_STRING
  if (from._internal_has_name()) {
    name_.Set(from._internal_name(), 
      GetArenaForAllocation());
  }
  display_name_.InitDefault();
  #ifdef PROTOBUF_FORCE_COPY_DEFAULT_STRING
    display_name_.Set("", GetArenaForAllocation());
  #endif // PROTOBUF_FORCE_COPY_DEFAULT_STRING
  if (from._internal_has_display_name()) {
    display_name_.Set(from._internal_display_name(), 
      GetArenaForAllocation());
  }
  id_ = from.id_;
  // @@protoc_insertion_point(copy_constructor:object_detection.protos.StringIntLabelMapItem)
}

inline void StringIntLabelMapItem::SharedCtor() {
name_.InitDefault();
#ifdef PROTOBUF_FORCE_COPY_DEFAULT_STRING
  name_.Set("", GetArenaForAllocation());
#endif // PROTOBUF_FORCE_COPY_DEFAULT_STRING
display_name_.InitDefault();
#ifdef PROTOBUF_FORCE_COPY_DEFAULT_STRING
  display_name_.Set("", GetArenaForAllocation());
#endif // PROTOBUF_FORCE_COPY_DEFAULT_STRING
id_ = 0;
}

StringIntLabelMapItem::~StringIntLabelMapItem() {
  // @@protoc_insertion_point(destructor:object_detection.protos.StringIntLabelMapItem)
  if (auto *arena = _internal_metadata_.DeleteReturnArena<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>()) {
  (void)arena;
    return;
  }
  SharedDtor();
}

inline void StringIntLabelMapItem::SharedDtor() {
  GOOGLE_DCHECK(GetArenaForAllocation() == nullptr);
  name_.Destroy();
  display_name_.Destroy();
}

void StringIntLabelMapItem::SetCachedSize(int size) const {
  _cached_size_.Set(size);
}

void StringIntLabelMapItem::Clear() {
// @@protoc_insertion_point(message_clear_start:object_detection.protos.StringIntLabelMapItem)
  uint32_t cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  cached_has_bits = _has_bits_[0];
  if (cached_has_bits & 0x00000003u) {
    if (cached_has_bits & 0x00000001u) {
      name_.ClearNonDefaultToEmpty();
    }
    if (cached_has_bits & 0x00000002u) {
      display_name_.ClearNonDefaultToEmpty();
    }
  }
  id_ = 0;
  _has_bits_.Clear();
  _internal_metadata_.Clear<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>();
}

const char* StringIntLabelMapItem::_InternalParse(const char* ptr, ::_pbi::ParseContext* ctx) {
#define CHK_(x) if (PROTOBUF_PREDICT_FALSE(!(x))) goto failure
  _Internal::HasBits has_bits{};
  while (!ctx->Done(&ptr)) {
    uint32_t tag;
    ptr = ::_pbi::ReadTag(ptr, &tag);
    switch (tag >> 3) {
      // optional string name = 1;
      case 1:
        if (PROTOBUF_PREDICT_TRUE(static_cast<uint8_t>(tag) == 10)) {
          auto str = _internal_mutable_name();
          ptr = ::_pbi::InlineGreedyStringParser(str, ptr, ctx);
          CHK_(ptr);
          #ifndef NDEBUG
          ::_pbi::VerifyUTF8(str, "object_detection.protos.StringIntLabelMapItem.name");
          #endif  // !NDEBUG
        } else
          goto handle_unusual;
        continue;
      // optional int32 id = 2;
      case 2:
        if (PROTOBUF_PREDICT_TRUE(static_cast<uint8_t>(tag) == 16)) {
          _Internal::set_has_id(&has_bits);
          id_ = ::PROTOBUF_NAMESPACE_ID::internal::ReadVarint32(&ptr);
          CHK_(ptr);
        } else
          goto handle_unusual;
        continue;
      // optional string display_name = 3;
      case 3:
        if (PROTOBUF_PREDICT_TRUE(static_cast<uint8_t>(tag) == 26)) {
          auto str = _internal_mutable_display_name();
          ptr = ::_pbi::InlineGreedyStringParser(str, ptr, ctx);
          CHK_(ptr);
          #ifndef NDEBUG
          ::_pbi::VerifyUTF8(str, "object_detection.protos.StringIntLabelMapItem.display_name");
          #endif  // !NDEBUG
        } else
          goto handle_unusual;
        continue;
      default:
        goto handle_unusual;
    }  // switch
  handle_unusual:
    if ((tag == 0) || ((tag & 7) == 4)) {
      CHK_(ptr);
      ctx->SetLastTag(tag);
      goto message_done;
    }
    ptr = UnknownFieldParse(
        tag,
        _internal_metadata_.mutable_unknown_fields<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(),
        ptr, ctx);
    CHK_(ptr != nullptr);
  }  // while
message_done:
  _has_bits_.Or(has_bits);
  return ptr;
failure:
  ptr = nullptr;
  goto message_done;
#undef CHK_
}

uint8_t* StringIntLabelMapItem::_InternalSerialize(
    uint8_t* target, ::PROTOBUF_NAMESPACE_ID::io::EpsCopyOutputStream* stream) const {
  // @@protoc_insertion_point(serialize_to_array_start:object_detection.protos.StringIntLabelMapItem)
  uint32_t cached_has_bits = 0;
  (void) cached_has_bits;

  cached_has_bits = _has_bits_[0];
  // optional string name = 1;
  if (cached_has_bits & 0x00000001u) {
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormat::VerifyUTF8StringNamedField(
      this->_internal_name().data(), static_cast<int>(this->_internal_name().length()),
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormat::SERIALIZE,
      "object_detection.protos.StringIntLabelMapItem.name");
    target = stream->WriteStringMaybeAliased(
        1, this->_internal_name(), target);
  }

  // optional int32 id = 2;
  if (cached_has_bits & 0x00000004u) {
    target = stream->EnsureSpace(target);
    target = ::_pbi::WireFormatLite::WriteInt32ToArray(2, this->_internal_id(), target);
  }

  // optional string display_name = 3;
  if (cached_has_bits & 0x00000002u) {
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormat::VerifyUTF8StringNamedField(
      this->_internal_display_name().data(), static_cast<int>(this->_internal_display_name().length()),
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormat::SERIALIZE,
      "object_detection.protos.StringIntLabelMapItem.display_name");
    target = stream->WriteStringMaybeAliased(
        3, this->_internal_display_name(), target);
  }

  if (PROTOBUF_PREDICT_FALSE(_internal_metadata_.have_unknown_fields())) {
    target = ::_pbi::WireFormat::InternalSerializeUnknownFieldsToArray(
        _internal_metadata_.unknown_fields<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(::PROTOBUF_NAMESPACE_ID::UnknownFieldSet::default_instance), target, stream);
  }
  // @@protoc_insertion_point(serialize_to_array_end:object_detection.protos.StringIntLabelMapItem)
  return target;
}

size_t StringIntLabelMapItem::ByteSizeLong() const {
// @@protoc_insertion_point(message_byte_size_start:object_detection.protos.StringIntLabelMapItem)
  size_t total_size = 0;

  uint32_t cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  cached_has_bits = _has_bits_[0];
  if (cached_has_bits & 0x00000007u) {
    // optional string name = 1;
    if (cached_has_bits & 0x00000001u) {
      total_size += 1 +
        ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::StringSize(
          this->_internal_name());
    }

    // optional string display_name = 3;
    if (cached_has_bits & 0x00000002u) {
      total_size += 1 +
        ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::StringSize(
          this->_internal_display_name());
    }

    // optional int32 id = 2;
    if (cached_has_bits & 0x00000004u) {
      total_size += ::_pbi::WireFormatLite::Int32SizePlusOne(this->_internal_id());
    }

  }
  return MaybeComputeUnknownFieldsSize(total_size, &_cached_size_);
}

const ::PROTOBUF_NAMESPACE_ID::Message::ClassData StringIntLabelMapItem::_class_data_ = {
    ::PROTOBUF_NAMESPACE_ID::Message::CopyWithSizeCheck,
    StringIntLabelMapItem::MergeImpl
};
const ::PROTOBUF_NAMESPACE_ID::Message::ClassData*StringIntLabelMapItem::GetClassData() const { return &_class_data_; }

void StringIntLabelMapItem::MergeImpl(::PROTOBUF_NAMESPACE_ID::Message* to,
                      const ::PROTOBUF_NAMESPACE_ID::Message& from) {
  static_cast<StringIntLabelMapItem *>(to)->MergeFrom(
      static_cast<const StringIntLabelMapItem &>(from));
}


void StringIntLabelMapItem::MergeFrom(const StringIntLabelMapItem& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:object_detection.protos.StringIntLabelMapItem)
  GOOGLE_DCHECK_NE(&from, this);
  uint32_t cached_has_bits = 0;
  (void) cached_has_bits;

  cached_has_bits = from._has_bits_[0];
  if (cached_has_bits & 0x00000007u) {
    if (cached_has_bits & 0x00000001u) {
      _internal_set_name(from._internal_name());
    }
    if (cached_has_bits & 0x00000002u) {
      _internal_set_display_name(from._internal_display_name());
    }
    if (cached_has_bits & 0x00000004u) {
      id_ = from.id_;
    }
    _has_bits_[0] |= cached_has_bits;
  }
  _internal_metadata_.MergeFrom<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(from._internal_metadata_);
}

void StringIntLabelMapItem::CopyFrom(const StringIntLabelMapItem& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:object_detection.protos.StringIntLabelMapItem)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool StringIntLabelMapItem::IsInitialized() const {
  return true;
}

void StringIntLabelMapItem::InternalSwap(StringIntLabelMapItem* other) {
  using std::swap;
  auto* lhs_arena = GetArenaForAllocation();
  auto* rhs_arena = other->GetArenaForAllocation();
  _internal_metadata_.InternalSwap(&other->_internal_metadata_);
  swap(_has_bits_[0], other->_has_bits_[0]);
  ::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr::InternalSwap(
      &name_, lhs_arena,
      &other->name_, rhs_arena
  );
  ::PROTOBUF_NAMESPACE_ID::internal::ArenaStringPtr::InternalSwap(
      &display_name_, lhs_arena,
      &other->display_name_, rhs_arena
  );
  swap(id_, other->id_);
}

::PROTOBUF_NAMESPACE_ID::Metadata StringIntLabelMapItem::GetMetadata() const {
  return ::_pbi::AssignDescriptors(
      &descriptor_table_object_5fdetection_2fprotos_2fstring_5fint_5flabel_5fmap_2eproto_getter, &descriptor_table_object_5fdetection_2fprotos_2fstring_5fint_5flabel_5fmap_2eproto_once,
      file_level_metadata_object_5fdetection_2fprotos_2fstring_5fint_5flabel_5fmap_2eproto[0]);
}

// ===================================================================

class StringIntLabelMap::_Internal {
 public:
};

StringIntLabelMap::StringIntLabelMap(::PROTOBUF_NAMESPACE_ID::Arena* arena,
                         bool is_message_owned)
  : ::PROTOBUF_NAMESPACE_ID::Message(arena, is_message_owned),
  item_(arena) {
  SharedCtor();
  // @@protoc_insertion_point(arena_constructor:object_detection.protos.StringIntLabelMap)
}
StringIntLabelMap::StringIntLabelMap(const StringIntLabelMap& from)
  : ::PROTOBUF_NAMESPACE_ID::Message(),
      item_(from.item_) {
  _internal_metadata_.MergeFrom<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(from._internal_metadata_);
  // @@protoc_insertion_point(copy_constructor:object_detection.protos.StringIntLabelMap)
}

inline void StringIntLabelMap::SharedCtor() {
}

StringIntLabelMap::~StringIntLabelMap() {
  // @@protoc_insertion_point(destructor:object_detection.protos.StringIntLabelMap)
  if (auto *arena = _internal_metadata_.DeleteReturnArena<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>()) {
  (void)arena;
    return;
  }
  SharedDtor();
}

inline void StringIntLabelMap::SharedDtor() {
  GOOGLE_DCHECK(GetArenaForAllocation() == nullptr);
}

void StringIntLabelMap::SetCachedSize(int size) const {
  _cached_size_.Set(size);
}

void StringIntLabelMap::Clear() {
// @@protoc_insertion_point(message_clear_start:object_detection.protos.StringIntLabelMap)
  uint32_t cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  item_.Clear();
  _internal_metadata_.Clear<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>();
}

const char* StringIntLabelMap::_InternalParse(const char* ptr, ::_pbi::ParseContext* ctx) {
#define CHK_(x) if (PROTOBUF_PREDICT_FALSE(!(x))) goto failure
  while (!ctx->Done(&ptr)) {
    uint32_t tag;
    ptr = ::_pbi::ReadTag(ptr, &tag);
    switch (tag >> 3) {
      // repeated .object_detection.protos.StringIntLabelMapItem item = 1;
      case 1:
        if (PROTOBUF_PREDICT_TRUE(static_cast<uint8_t>(tag) == 10)) {
          ptr -= 1;
          do {
            ptr += 1;
            ptr = ctx->ParseMessage(_internal_add_item(), ptr);
            CHK_(ptr);
            if (!ctx->DataAvailable(ptr)) break;
          } while (::PROTOBUF_NAMESPACE_ID::internal::ExpectTag<10>(ptr));
        } else
          goto handle_unusual;
        continue;
      default:
        goto handle_unusual;
    }  // switch
  handle_unusual:
    if ((tag == 0) || ((tag & 7) == 4)) {
      CHK_(ptr);
      ctx->SetLastTag(tag);
      goto message_done;
    }
    ptr = UnknownFieldParse(
        tag,
        _internal_metadata_.mutable_unknown_fields<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(),
        ptr, ctx);
    CHK_(ptr != nullptr);
  }  // while
message_done:
  return ptr;
failure:
  ptr = nullptr;
  goto message_done;
#undef CHK_
}

uint8_t* StringIntLabelMap::_InternalSerialize(
    uint8_t* target, ::PROTOBUF_NAMESPACE_ID::io::EpsCopyOutputStream* stream) const {
  // @@protoc_insertion_point(serialize_to_array_start:object_detection.protos.StringIntLabelMap)
  uint32_t cached_has_bits = 0;
  (void) cached_has_bits;

  // repeated .object_detection.protos.StringIntLabelMapItem item = 1;
  for (unsigned i = 0,
      n = static_cast<unsigned>(this->_internal_item_size()); i < n; i++) {
    const auto& repfield = this->_internal_item(i);
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::
        InternalWriteMessage(1, repfield, repfield.GetCachedSize(), target, stream);
  }

  if (PROTOBUF_PREDICT_FALSE(_internal_metadata_.have_unknown_fields())) {
    target = ::_pbi::WireFormat::InternalSerializeUnknownFieldsToArray(
        _internal_metadata_.unknown_fields<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(::PROTOBUF_NAMESPACE_ID::UnknownFieldSet::default_instance), target, stream);
  }
  // @@protoc_insertion_point(serialize_to_array_end:object_detection.protos.StringIntLabelMap)
  return target;
}

size_t StringIntLabelMap::ByteSizeLong() const {
// @@protoc_insertion_point(message_byte_size_start:object_detection.protos.StringIntLabelMap)
  size_t total_size = 0;

  uint32_t cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  // repeated .object_detection.protos.StringIntLabelMapItem item = 1;
  total_size += 1UL * this->_internal_item_size();
  for (const auto& msg : this->item_) {
    total_size +=
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::MessageSize(msg);
  }

  return MaybeComputeUnknownFieldsSize(total_size, &_cached_size_);
}

const ::PROTOBUF_NAMESPACE_ID::Message::ClassData StringIntLabelMap::_class_data_ = {
    ::PROTOBUF_NAMESPACE_ID::Message::CopyWithSizeCheck,
    StringIntLabelMap::MergeImpl
};
const ::PROTOBUF_NAMESPACE_ID::Message::ClassData*StringIntLabelMap::GetClassData() const { return &_class_data_; }

void StringIntLabelMap::MergeImpl(::PROTOBUF_NAMESPACE_ID::Message* to,
                      const ::PROTOBUF_NAMESPACE_ID::Message& from) {
  static_cast<StringIntLabelMap *>(to)->MergeFrom(
      static_cast<const StringIntLabelMap &>(from));
}


void StringIntLabelMap::MergeFrom(const StringIntLabelMap& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:object_detection.protos.StringIntLabelMap)
  GOOGLE_DCHECK_NE(&from, this);
  uint32_t cached_has_bits = 0;
  (void) cached_has_bits;

  item_.MergeFrom(from.item_);
  _internal_metadata_.MergeFrom<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(from._internal_metadata_);
}

void StringIntLabelMap::CopyFrom(const StringIntLabelMap& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:object_detection.protos.StringIntLabelMap)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool StringIntLabelMap::IsInitialized() const {
  return true;
}

void StringIntLabelMap::InternalSwap(StringIntLabelMap* other) {
  using std::swap;
  _internal_metadata_.InternalSwap(&other->_internal_metadata_);
  item_.InternalSwap(&other->item_);
}

::PROTOBUF_NAMESPACE_ID::Metadata StringIntLabelMap::GetMetadata() const {
  return ::_pbi::AssignDescriptors(
      &descriptor_table_object_5fdetection_2fprotos_2fstring_5fint_5flabel_5fmap_2eproto_getter, &descriptor_table_object_5fdetection_2fprotos_2fstring_5fint_5flabel_5fmap_2eproto_once,
      file_level_metadata_object_5fdetection_2fprotos_2fstring_5fint_5flabel_5fmap_2eproto[1]);
}

// @@protoc_insertion_point(namespace_scope)
}  // namespace protos
}  // namespace object_detection
PROTOBUF_NAMESPACE_OPEN
template<> PROTOBUF_NOINLINE ::object_detection::protos::StringIntLabelMapItem*
Arena::CreateMaybeMessage< ::object_detection::protos::StringIntLabelMapItem >(Arena* arena) {
  return Arena::CreateMessageInternal< ::object_detection::protos::StringIntLabelMapItem >(arena);
}
template<> PROTOBUF_NOINLINE ::object_detection::protos::StringIntLabelMap*
Arena::CreateMaybeMessage< ::object_detection::protos::StringIntLabelMap >(Arena* arena) {
  return Arena::CreateMessageInternal< ::object_detection::protos::StringIntLabelMap >(arena);
}
PROTOBUF_NAMESPACE_CLOSE

// @@protoc_insertion_point(global_scope)
#include <google/protobuf/port_undef.inc>
