// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: object_detection/protos/faster_rcnn_box_coder.proto

#include "object_detection/protos/faster_rcnn_box_coder.pb.h"

#include <algorithm>

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/wire_format_lite.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/reflection_ops.h>
#include <google/protobuf/wire_format.h>
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>

PROTOBUF_PRAGMA_INIT_SEG

namespace _pb = ::PROTOBUF_NAMESPACE_ID;
namespace _pbi = _pb::internal;

namespace object_detection {
namespace protos {
PROTOBUF_CONSTEXPR FasterRcnnBoxCoder::FasterRcnnBoxCoder(
    ::_pbi::ConstantInitialized)
  : y_scale_(10)
  , x_scale_(10)
  , height_scale_(5)
  , width_scale_(5){}
struct FasterRcnnBoxCoderDefaultTypeInternal {
  PROTOBUF_CONSTEXPR FasterRcnnBoxCoderDefaultTypeInternal()
      : _instance(::_pbi::ConstantInitialized{}) {}
  ~FasterRcnnBoxCoderDefaultTypeInternal() {}
  union {
    FasterRcnnBoxCoder _instance;
  };
};
PROTOBUF_ATTRIBUTE_NO_DESTROY PROTOBUF_CONSTINIT PROTOBUF_ATTRIBUTE_INIT_PRIORITY1 FasterRcnnBoxCoderDefaultTypeInternal _FasterRcnnBoxCoder_default_instance_;
}  // namespace protos
}  // namespace object_detection
static ::_pb::Metadata file_level_metadata_object_5fdetection_2fprotos_2ffaster_5frcnn_5fbox_5fcoder_2eproto[1];
static constexpr ::_pb::EnumDescriptor const** file_level_enum_descriptors_object_5fdetection_2fprotos_2ffaster_5frcnn_5fbox_5fcoder_2eproto = nullptr;
static constexpr ::_pb::ServiceDescriptor const** file_level_service_descriptors_object_5fdetection_2fprotos_2ffaster_5frcnn_5fbox_5fcoder_2eproto = nullptr;

const uint32_t TableStruct_object_5fdetection_2fprotos_2ffaster_5frcnn_5fbox_5fcoder_2eproto::offsets[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) = {
  PROTOBUF_FIELD_OFFSET(::object_detection::protos::FasterRcnnBoxCoder, _has_bits_),
  PROTOBUF_FIELD_OFFSET(::object_detection::protos::FasterRcnnBoxCoder, _internal_metadata_),
  ~0u,  // no _extensions_
  ~0u,  // no _oneof_case_
  ~0u,  // no _weak_field_map_
  ~0u,  // no _inlined_string_donated_
  PROTOBUF_FIELD_OFFSET(::object_detection::protos::FasterRcnnBoxCoder, y_scale_),
  PROTOBUF_FIELD_OFFSET(::object_detection::protos::FasterRcnnBoxCoder, x_scale_),
  PROTOBUF_FIELD_OFFSET(::object_detection::protos::FasterRcnnBoxCoder, height_scale_),
  PROTOBUF_FIELD_OFFSET(::object_detection::protos::FasterRcnnBoxCoder, width_scale_),
  0,
  1,
  2,
  3,
};
static const ::_pbi::MigrationSchema schemas[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) = {
  { 0, 10, -1, sizeof(::object_detection::protos::FasterRcnnBoxCoder)},
};

static const ::_pb::Message* const file_default_instances[] = {
  &::object_detection::protos::_FasterRcnnBoxCoder_default_instance_._instance,
};

const char descriptor_table_protodef_object_5fdetection_2fprotos_2ffaster_5frcnn_5fbox_5fcoder_2eproto[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) =
  "\n3object_detection/protos/faster_rcnn_bo"
  "x_coder.proto\022\027object_detection.protos\"o"
  "\n\022FasterRcnnBoxCoder\022\023\n\007y_scale\030\001 \001(\002:\0021"
  "0\022\023\n\007x_scale\030\002 \001(\002:\00210\022\027\n\014height_scale\030\003"
  " \001(\002:\0015\022\026\n\013width_scale\030\004 \001(\002:\0015"
  ;
static ::_pbi::once_flag descriptor_table_object_5fdetection_2fprotos_2ffaster_5frcnn_5fbox_5fcoder_2eproto_once;
const ::_pbi::DescriptorTable descriptor_table_object_5fdetection_2fprotos_2ffaster_5frcnn_5fbox_5fcoder_2eproto = {
    false, false, 191, descriptor_table_protodef_object_5fdetection_2fprotos_2ffaster_5frcnn_5fbox_5fcoder_2eproto,
    "object_detection/protos/faster_rcnn_box_coder.proto",
    &descriptor_table_object_5fdetection_2fprotos_2ffaster_5frcnn_5fbox_5fcoder_2eproto_once, nullptr, 0, 1,
    schemas, file_default_instances, TableStruct_object_5fdetection_2fprotos_2ffaster_5frcnn_5fbox_5fcoder_2eproto::offsets,
    file_level_metadata_object_5fdetection_2fprotos_2ffaster_5frcnn_5fbox_5fcoder_2eproto, file_level_enum_descriptors_object_5fdetection_2fprotos_2ffaster_5frcnn_5fbox_5fcoder_2eproto,
    file_level_service_descriptors_object_5fdetection_2fprotos_2ffaster_5frcnn_5fbox_5fcoder_2eproto,
};
PROTOBUF_ATTRIBUTE_WEAK const ::_pbi::DescriptorTable* descriptor_table_object_5fdetection_2fprotos_2ffaster_5frcnn_5fbox_5fcoder_2eproto_getter() {
  return &descriptor_table_object_5fdetection_2fprotos_2ffaster_5frcnn_5fbox_5fcoder_2eproto;
}

// Force running AddDescriptors() at dynamic initialization time.
PROTOBUF_ATTRIBUTE_INIT_PRIORITY2 static ::_pbi::AddDescriptorsRunner dynamic_init_dummy_object_5fdetection_2fprotos_2ffaster_5frcnn_5fbox_5fcoder_2eproto(&descriptor_table_object_5fdetection_2fprotos_2ffaster_5frcnn_5fbox_5fcoder_2eproto);
namespace object_detection {
namespace protos {

// ===================================================================

class FasterRcnnBoxCoder::_Internal {
 public:
  using HasBits = decltype(std::declval<FasterRcnnBoxCoder>()._has_bits_);
  static void set_has_y_scale(HasBits* has_bits) {
    (*has_bits)[0] |= 1u;
  }
  static void set_has_x_scale(HasBits* has_bits) {
    (*has_bits)[0] |= 2u;
  }
  static void set_has_height_scale(HasBits* has_bits) {
    (*has_bits)[0] |= 4u;
  }
  static void set_has_width_scale(HasBits* has_bits) {
    (*has_bits)[0] |= 8u;
  }
};

FasterRcnnBoxCoder::FasterRcnnBoxCoder(::PROTOBUF_NAMESPACE_ID::Arena* arena,
                         bool is_message_owned)
  : ::PROTOBUF_NAMESPACE_ID::Message(arena, is_message_owned) {
  SharedCtor();
  // @@protoc_insertion_point(arena_constructor:object_detection.protos.FasterRcnnBoxCoder)
}
FasterRcnnBoxCoder::FasterRcnnBoxCoder(const FasterRcnnBoxCoder& from)
  : ::PROTOBUF_NAMESPACE_ID::Message(),
      _has_bits_(from._has_bits_) {
  _internal_metadata_.MergeFrom<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(from._internal_metadata_);
  ::memcpy(&y_scale_, &from.y_scale_,
    static_cast<size_t>(reinterpret_cast<char*>(&width_scale_) -
    reinterpret_cast<char*>(&y_scale_)) + sizeof(width_scale_));
  // @@protoc_insertion_point(copy_constructor:object_detection.protos.FasterRcnnBoxCoder)
}

inline void FasterRcnnBoxCoder::SharedCtor() {
y_scale_ = 10;
x_scale_ = 10;
height_scale_ = 5;
width_scale_ = 5;
}

FasterRcnnBoxCoder::~FasterRcnnBoxCoder() {
  // @@protoc_insertion_point(destructor:object_detection.protos.FasterRcnnBoxCoder)
  if (auto *arena = _internal_metadata_.DeleteReturnArena<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>()) {
  (void)arena;
    return;
  }
  SharedDtor();
}

inline void FasterRcnnBoxCoder::SharedDtor() {
  GOOGLE_DCHECK(GetArenaForAllocation() == nullptr);
}

void FasterRcnnBoxCoder::SetCachedSize(int size) const {
  _cached_size_.Set(size);
}

void FasterRcnnBoxCoder::Clear() {
// @@protoc_insertion_point(message_clear_start:object_detection.protos.FasterRcnnBoxCoder)
  uint32_t cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  cached_has_bits = _has_bits_[0];
  if (cached_has_bits & 0x0000000fu) {
    y_scale_ = 10;
    x_scale_ = 10;
    height_scale_ = 5;
    width_scale_ = 5;
  }
  _has_bits_.Clear();
  _internal_metadata_.Clear<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>();
}

const char* FasterRcnnBoxCoder::_InternalParse(const char* ptr, ::_pbi::ParseContext* ctx) {
#define CHK_(x) if (PROTOBUF_PREDICT_FALSE(!(x))) goto failure
  _Internal::HasBits has_bits{};
  while (!ctx->Done(&ptr)) {
    uint32_t tag;
    ptr = ::_pbi::ReadTag(ptr, &tag);
    switch (tag >> 3) {
      // optional float y_scale = 1 [default = 10];
      case 1:
        if (PROTOBUF_PREDICT_TRUE(static_cast<uint8_t>(tag) == 13)) {
          _Internal::set_has_y_scale(&has_bits);
          y_scale_ = ::PROTOBUF_NAMESPACE_ID::internal::UnalignedLoad<float>(ptr);
          ptr += sizeof(float);
        } else
          goto handle_unusual;
        continue;
      // optional float x_scale = 2 [default = 10];
      case 2:
        if (PROTOBUF_PREDICT_TRUE(static_cast<uint8_t>(tag) == 21)) {
          _Internal::set_has_x_scale(&has_bits);
          x_scale_ = ::PROTOBUF_NAMESPACE_ID::internal::UnalignedLoad<float>(ptr);
          ptr += sizeof(float);
        } else
          goto handle_unusual;
        continue;
      // optional float height_scale = 3 [default = 5];
      case 3:
        if (PROTOBUF_PREDICT_TRUE(static_cast<uint8_t>(tag) == 29)) {
          _Internal::set_has_height_scale(&has_bits);
          height_scale_ = ::PROTOBUF_NAMESPACE_ID::internal::UnalignedLoad<float>(ptr);
          ptr += sizeof(float);
        } else
          goto handle_unusual;
        continue;
      // optional float width_scale = 4 [default = 5];
      case 4:
        if (PROTOBUF_PREDICT_TRUE(static_cast<uint8_t>(tag) == 37)) {
          _Internal::set_has_width_scale(&has_bits);
          width_scale_ = ::PROTOBUF_NAMESPACE_ID::internal::UnalignedLoad<float>(ptr);
          ptr += sizeof(float);
        } else
          goto handle_unusual;
        continue;
      default:
        goto handle_unusual;
    }  // switch
  handle_unusual:
    if ((tag == 0) || ((tag & 7) == 4)) {
      CHK_(ptr);
      ctx->SetLastTag(tag);
      goto message_done;
    }
    ptr = UnknownFieldParse(
        tag,
        _internal_metadata_.mutable_unknown_fields<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(),
        ptr, ctx);
    CHK_(ptr != nullptr);
  }  // while
message_done:
  _has_bits_.Or(has_bits);
  return ptr;
failure:
  ptr = nullptr;
  goto message_done;
#undef CHK_
}

uint8_t* FasterRcnnBoxCoder::_InternalSerialize(
    uint8_t* target, ::PROTOBUF_NAMESPACE_ID::io::EpsCopyOutputStream* stream) const {
  // @@protoc_insertion_point(serialize_to_array_start:object_detection.protos.FasterRcnnBoxCoder)
  uint32_t cached_has_bits = 0;
  (void) cached_has_bits;

  cached_has_bits = _has_bits_[0];
  // optional float y_scale = 1 [default = 10];
  if (cached_has_bits & 0x00000001u) {
    target = stream->EnsureSpace(target);
    target = ::_pbi::WireFormatLite::WriteFloatToArray(1, this->_internal_y_scale(), target);
  }

  // optional float x_scale = 2 [default = 10];
  if (cached_has_bits & 0x00000002u) {
    target = stream->EnsureSpace(target);
    target = ::_pbi::WireFormatLite::WriteFloatToArray(2, this->_internal_x_scale(), target);
  }

  // optional float height_scale = 3 [default = 5];
  if (cached_has_bits & 0x00000004u) {
    target = stream->EnsureSpace(target);
    target = ::_pbi::WireFormatLite::WriteFloatToArray(3, this->_internal_height_scale(), target);
  }

  // optional float width_scale = 4 [default = 5];
  if (cached_has_bits & 0x00000008u) {
    target = stream->EnsureSpace(target);
    target = ::_pbi::WireFormatLite::WriteFloatToArray(4, this->_internal_width_scale(), target);
  }

  if (PROTOBUF_PREDICT_FALSE(_internal_metadata_.have_unknown_fields())) {
    target = ::_pbi::WireFormat::InternalSerializeUnknownFieldsToArray(
        _internal_metadata_.unknown_fields<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(::PROTOBUF_NAMESPACE_ID::UnknownFieldSet::default_instance), target, stream);
  }
  // @@protoc_insertion_point(serialize_to_array_end:object_detection.protos.FasterRcnnBoxCoder)
  return target;
}

size_t FasterRcnnBoxCoder::ByteSizeLong() const {
// @@protoc_insertion_point(message_byte_size_start:object_detection.protos.FasterRcnnBoxCoder)
  size_t total_size = 0;

  uint32_t cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  cached_has_bits = _has_bits_[0];
  if (cached_has_bits & 0x0000000fu) {
    // optional float y_scale = 1 [default = 10];
    if (cached_has_bits & 0x00000001u) {
      total_size += 1 + 4;
    }

    // optional float x_scale = 2 [default = 10];
    if (cached_has_bits & 0x00000002u) {
      total_size += 1 + 4;
    }

    // optional float height_scale = 3 [default = 5];
    if (cached_has_bits & 0x00000004u) {
      total_size += 1 + 4;
    }

    // optional float width_scale = 4 [default = 5];
    if (cached_has_bits & 0x00000008u) {
      total_size += 1 + 4;
    }

  }
  return MaybeComputeUnknownFieldsSize(total_size, &_cached_size_);
}

const ::PROTOBUF_NAMESPACE_ID::Message::ClassData FasterRcnnBoxCoder::_class_data_ = {
    ::PROTOBUF_NAMESPACE_ID::Message::CopyWithSizeCheck,
    FasterRcnnBoxCoder::MergeImpl
};
const ::PROTOBUF_NAMESPACE_ID::Message::ClassData*FasterRcnnBoxCoder::GetClassData() const { return &_class_data_; }

void FasterRcnnBoxCoder::MergeImpl(::PROTOBUF_NAMESPACE_ID::Message* to,
                      const ::PROTOBUF_NAMESPACE_ID::Message& from) {
  static_cast<FasterRcnnBoxCoder *>(to)->MergeFrom(
      static_cast<const FasterRcnnBoxCoder &>(from));
}


void FasterRcnnBoxCoder::MergeFrom(const FasterRcnnBoxCoder& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:object_detection.protos.FasterRcnnBoxCoder)
  GOOGLE_DCHECK_NE(&from, this);
  uint32_t cached_has_bits = 0;
  (void) cached_has_bits;

  cached_has_bits = from._has_bits_[0];
  if (cached_has_bits & 0x0000000fu) {
    if (cached_has_bits & 0x00000001u) {
      y_scale_ = from.y_scale_;
    }
    if (cached_has_bits & 0x00000002u) {
      x_scale_ = from.x_scale_;
    }
    if (cached_has_bits & 0x00000004u) {
      height_scale_ = from.height_scale_;
    }
    if (cached_has_bits & 0x00000008u) {
      width_scale_ = from.width_scale_;
    }
    _has_bits_[0] |= cached_has_bits;
  }
  _internal_metadata_.MergeFrom<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(from._internal_metadata_);
}

void FasterRcnnBoxCoder::CopyFrom(const FasterRcnnBoxCoder& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:object_detection.protos.FasterRcnnBoxCoder)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool FasterRcnnBoxCoder::IsInitialized() const {
  return true;
}

void FasterRcnnBoxCoder::InternalSwap(FasterRcnnBoxCoder* other) {
  using std::swap;
  _internal_metadata_.InternalSwap(&other->_internal_metadata_);
  swap(_has_bits_[0], other->_has_bits_[0]);
  swap(y_scale_, other->y_scale_);
  swap(x_scale_, other->x_scale_);
  swap(height_scale_, other->height_scale_);
  swap(width_scale_, other->width_scale_);
}

::PROTOBUF_NAMESPACE_ID::Metadata FasterRcnnBoxCoder::GetMetadata() const {
  return ::_pbi::AssignDescriptors(
      &descriptor_table_object_5fdetection_2fprotos_2ffaster_5frcnn_5fbox_5fcoder_2eproto_getter, &descriptor_table_object_5fdetection_2fprotos_2ffaster_5frcnn_5fbox_5fcoder_2eproto_once,
      file_level_metadata_object_5fdetection_2fprotos_2ffaster_5frcnn_5fbox_5fcoder_2eproto[0]);
}

// @@protoc_insertion_point(namespace_scope)
}  // namespace protos
}  // namespace object_detection
PROTOBUF_NAMESPACE_OPEN
template<> PROTOBUF_NOINLINE ::object_detection::protos::FasterRcnnBoxCoder*
Arena::CreateMaybeMessage< ::object_detection::protos::FasterRcnnBoxCoder >(Arena* arena) {
  return Arena::CreateMessageInternal< ::object_detection::protos::FasterRcnnBoxCoder >(arena);
}
PROTOBUF_NAMESPACE_CLOSE

// @@protoc_insertion_point(global_scope)
#include <google/protobuf/port_undef.inc>
