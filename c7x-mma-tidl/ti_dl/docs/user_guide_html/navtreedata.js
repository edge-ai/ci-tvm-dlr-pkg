/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "TI Deep Learning Product User Guide", "index.html", [
    [ "TIDL - TI Deep Learning Product", "index.html", null ],
    [ "TIDL Runtime", "usergroup0.html", [
      [ "TIDL-RT Overview", "md_tidl_overview.html", null ],
      [ "TIDL-RT Build Instructions", "usergroup1.html", [
        [ "Dependant software components", "md_tidl_dependency_info.html", null ],
        [ "Build Instructions", "md_tidl_build_instruction.html", null ]
      ] ],
      [ "API Guide", "usergroup2.html", [
        [ "TIDL-RT    API Guide", "itidl__rt_8h.html", null ],
        [ "TIDL-LIB   API Guide", "itidl__ti_8h.html", null ]
      ] ]
    ] ],
    [ "TIDL Package Contents", "md_tidl_package_contents.html", null ],
    [ "Acronyms", "md_tidl_acronyms.html", null ],
    [ "TI Disclaimer", "md_ti_disclaimer.html", null ]
  ] ]
];

var NAVTREEINDEX =
[
"index.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';