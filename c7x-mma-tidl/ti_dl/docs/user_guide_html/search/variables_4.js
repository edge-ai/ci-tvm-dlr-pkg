var searchData=
[
  ['elementtype',['elementType',['../structsTIDLRT__Tensor__t.html#a6eafe3db5ced89f680bd1e1c0d2c1ce4',1,'sTIDLRT_Tensor_t::elementType()'],['../structsTIDL__DataParams__t.html#a11f31740708d101e4e4582d4546797a6',1,'sTIDL_DataParams_t::elementType()']]],
  ['eltwiseparams',['eltWiseParams',['../unionsTIDL__LayerParams__t.html#ac98bb3e1fc3acb923b584572aed4b7e7',1,'sTIDL_LayerParams_t']]],
  ['eltwisetype',['eltWiseType',['../structsTIDL__EltWiseParams__t.html#a2d5816309076f7827d4c9e503ab2dca4',1,'sTIDL_EltWiseParams_t']]],
  ['enablebias',['enableBias',['../structsTIDL__ConvParams__t.html#a7342df27134a41161f9ca8f7a4363ca6',1,'sTIDL_ConvParams_t']]],
  ['enabledepthtospace',['enableDepthToSpace',['../structsTIDL__ConvParams__t.html#afe5a387b940f32bde5c50783ff5ba1c7',1,'sTIDL_ConvParams_t']]],
  ['enableeltwise',['enableEltWise',['../structsTIDL__ConvParams__t.html#af415a43f8d33aba2c8baf7bdfd107835',1,'sTIDL_ConvParams_t']]],
  ['enableewrelu',['enableEWRelU',['../structsTIDL__ConvParams__t.html#a9ce8b3d960400219e8928f0e28ae9ded',1,'sTIDL_ConvParams_t']]],
  ['enablelayerperftraces',['enableLayerPerfTraces',['../structTIDL__InArgs.html#a8c55dfe119f816275e3c7bafcda0de51',1,'TIDL_InArgs']]],
  ['enablepooling',['enablePooling',['../structsTIDL__ConvParams__t.html#adc4fce67eb4636d794e985628a7d48d8',1,'sTIDL_ConvParams_t']]],
  ['epsilon',['epsilon',['../structsTIDL__LayerNormParams__t.html#a4dee850bc64a471ca828627ec146bcc4',1,'sTIDL_LayerNormParams_t']]],
  ['eta',['eta',['../structsTIDL__DetectOutputParams__t.html#a6405e050dc921de07e2a2643c0dff0b3',1,'sTIDL_DetectOutputParams_t']]],
  ['execfuncptr',['execFuncPtr',['../structTIDL__DataflowInitParams.html#a7f7b08924a6d2d7c4f6c6a5d89516965',1,'TIDL_DataflowInitParams']]]
];
