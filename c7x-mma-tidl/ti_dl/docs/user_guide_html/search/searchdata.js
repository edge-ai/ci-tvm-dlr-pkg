var indexSectionsWithContent =
{
  0: "abcdefghiklmnopqrstuvwxyz",
  1: "st",
  2: "irt",
  3: "t",
  4: "abcdefghiklmnopqrstuvwxyz",
  5: "fst",
  6: "e",
  7: "mt",
  8: "tu",
  9: "abdot"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Pages"
};

