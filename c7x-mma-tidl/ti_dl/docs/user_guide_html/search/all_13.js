var searchData=
[
  ['udmadrvobj',['udmaDrvObj',['../structTIDL__CreateParams.html#a0ff705355ff7c4bd37b28ca731080bd7',1,'TIDL_CreateParams']]],
  ['updatetensorname',['updateTensorName',['../structsTIDL__ScatterElementsParams__t.html#a887888e3b8f4a3d67203e28786e3bf8b',1,'sTIDL_ScatterElementsParams_t']]],
  ['upscalefactor',['upscaleFactor',['../structsTIDL__ConvParams__t.html#a593d5ad57a76a445c1212bd196c8788d',1,'sTIDL_ConvParams_t']]],
  ['use_5fivision',['USE_IVISION',['../itidl__ti_8h.html#ad2fd0d64e5563ae995fc9fc92b3e0632',1,'itidl_ti.h']]],
  ['useceil',['useCeil',['../structsTIDL__PoolingParams__t.html#ab37cb0d1baf63e3071384123a474b84e',1,'sTIDL_PoolingParams_t']]]
];
