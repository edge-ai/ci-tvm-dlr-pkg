var searchData=
[
  ['z',['z',['../structTIDL__3DODLayerObjInfo.html#a36eb872f82b366d01182b76d9e4e1fec',1,'TIDL_3DODLayerObjInfo']]],
  ['zeropoint',['zeroPoint',['../structsTIDLRT__Tensor__t.html#a209dc9ab9a665a4a3fdcff6cc59012f8',1,'sTIDLRT_Tensor_t']]],
  ['zeroweightvalue',['zeroWeightValue',['../structsTIDL__ConvParams__t.html#aa90e3f403375c0a9d25fadfeb566d8df',1,'sTIDL_ConvParams_t::zeroWeightValue()'],['../structsTIDL__BatchNormParams__t.html#abf4f7258aca4512b777f05af396f4347',1,'sTIDL_BatchNormParams_t::zeroWeightValue()'],['../structsTIDL__InnerProductParams__t.html#a5622b0a5641525faf7778ad831897f11',1,'sTIDL_InnerProductParams_t::zeroWeightValue()']]]
];
