var searchData=
[
  ['keepdims',['keepDims',['../structsTIDL__ReduceParams__t.html#a8525ae04451a8589ee591ad5594098aa',1,'sTIDL_ReduceParams_t']]],
  ['keeptopk',['keepTopK',['../structsTIDL__DetectOutputParams__t.html#adab4bd1da4fa05ac1ecdaed26f1063fb',1,'sTIDL_DetectOutputParams_t']]],
  ['kernelh',['kernelH',['../structsTIDL__PoolingParams__t.html#a60a34c41cc6aaf2dd07f5baa05231cee',1,'sTIDL_PoolingParams_t::kernelH()'],['../structsTIDL__ConvParams__t.html#a3e8eb4810f09cfedc1539168bbaca695',1,'sTIDL_ConvParams_t::kernelH()']]],
  ['kernelinitargs',['kernelInitArgs',['../structTIDL__DataflowInitParams.html#ac15f38a64a1affaec26f364bd91806b4',1,'TIDL_DataflowInitParams']]],
  ['kerneltype',['kernelType',['../structsTIDL__ConvParams__t.html#a17f642089dde7c43e565f9417da4e6a2',1,'sTIDL_ConvParams_t']]],
  ['kernelw',['kernelW',['../structsTIDL__PoolingParams__t.html#a88be13db1e322b657a6fe9e894cd3494',1,'sTIDL_PoolingParams_t::kernelW()'],['../structsTIDL__ConvParams__t.html#acfe36dc28a9072d45e5bd3c829be2cc8',1,'sTIDL_ConvParams_t::kernelW()']]],
  ['keypointconfidence',['keypointConfidence',['../structsTIDL__DetectOutputParams__t.html#a91a65b175540684142f4fcde9ca1d6f0',1,'sTIDL_DetectOutputParams_t']]],
  ['keypoints',['keyPoints',['../structTIDL__ODLayerObjInfo.html#abf3c18aca7822a5fadcc80d9c83d9fea',1,'TIDL_ODLayerObjInfo']]],
  ['kpscales',['kpScales',['../structsTIDL__AnchorBoxParams__t.html#a817b4cbac03d4653d04ebabd40bf3e51',1,'sTIDL_AnchorBoxParams_t']]],
  ['kpt_5fconfidence',['kpt_confidence',['../structTIDL__ODLayerKeyPoint.html#aeded9aa0694317a3f962c62f9d2a599f',1,'TIDL_ODLayerKeyPoint']]]
];
