var searchData=
[
  ['activationrangemethod',['activationRangeMethod',['../structsTIDL__CalibParams__t.html#af5941cf243323852feb9d9c6bdc51aba',1,'sTIDL_CalibParams_t']]],
  ['activationtype',['activationType',['../structsTIDL__InnerProductParams__t.html#a84fe5af9aa7c7e2ecaa95a58813a04af',1,'sTIDL_InnerProductParams_t']]],
  ['actparams',['actParams',['../structsTIDL__Layer__t.html#a42d681b77eec011d3ca8c6335b97c00b',1,'sTIDL_Layer_t']]],
  ['acttype',['actType',['../structsTIDL__ActParams__t.html#abba3d58de3ee27b73f6d723e38787d5f',1,'sTIDL_ActParams_t']]],
  ['actualops',['actualOps',['../structTIDL__LayerMetaData.html#a23652d20cbae7f8319e61a919541ac27',1,'TIDL_LayerMetaData']]],
  ['anchorinputs',['anchorInputs',['../structsTIDL__AnchorBoxParams__t.html#ac9015af8d12cbb9a49c43fbf6eb4fef0',1,'sTIDL_AnchorBoxParams_t']]],
  ['argmaxparams',['argMaxParams',['../unionsTIDL__LayerParams__t.html#aae25e6d601e0e5d8ab354c826b8fd133',1,'sTIDL_LayerParams_t']]],
  ['avgdims',['avgDims',['../structsTIDL__PoolingParams__t.html#aeabf4f0a17525784fca948be35c70d00',1,'sTIDL_PoolingParams_t']]],
  ['axis',['axis',['../structsTIDL__LayerNormParams__t.html#ab2da943de552162dca6429004e4d616a',1,'sTIDL_LayerNormParams_t::axis()'],['../structsTIDL__GatherLayerParams__t.html#ad687bdf8773465987eb1dbb73ae263d4',1,'sTIDL_GatherLayerParams_t::axis()'],['../structsTIDL__SliceLayerParams__t.html#afa801d57533440c6388f8d9c22be7b1f',1,'sTIDL_SliceLayerParams_t::axis()'],['../structsTIDL__ConcatParams__t.html#a6ad49f60f0de45feb4a22ed98f41e60a',1,'sTIDL_ConcatParams_t::axis()'],['../structsTIDL__SoftMaxParams__t.html#aa64886203fa6f8b6ad697cda76af6f36',1,'sTIDL_SoftMaxParams_t::axis()'],['../structsTIDL__ReduceParams__t.html#a7f0be6de761b9c791ac78efd29bc9023',1,'sTIDL_ReduceParams_t::axis()'],['../structsTIDL__ScatterElementsParams__t.html#aa23ce9e6dd26ac857b57f1ca4b0b689f',1,'sTIDL_ScatterElementsParams_t::axis()']]]
];
