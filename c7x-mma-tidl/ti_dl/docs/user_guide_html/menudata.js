/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var menudata={children:[
{text:"Main Page",url:"index.html"},
{text:"TIDL Runtime",url:"usergroup0.html",children:[
{text:"TIDL-RT Overview",url:"md_tidl_overview.html"},
{text:"TIDL-RT Build Instructions",url:"usergroup1.html",children:[
{text:"Dependant software components",url:"md_tidl_dependency_info.html"},
{text:"Build Instructions",url:"md_tidl_build_instruction.html"}]},
{text:"API Guide",url:"usergroup2.html",children:[
{text:"TIDL-RT    API Guide",url:"itidl__rt_8h.html"},
{text:"TIDL-LIB   API Guide",url:"itidl__ti_8h.html"}]}]},
{text:"TIDL Package Contents",url:"md_tidl_package_contents.html"},
{text:"Acronyms",url:"md_tidl_acronyms.html"},
{text:"TI Disclaimer",url:"md_ti_disclaimer.html"}]}
