sudo apt install clang llvm-dev lld lldb
 
TVM_TAG=TIDL_PSDK_9.2.0
DLR_TAG=TIDL_PSDK_9.2.0
 
export PSDKR_PATH=$HOME/psdk_vision/j721e  # SDK build workarea
export TIDL_DIR_NAME=c7x-mma-tidl   # alt: tidl_j7*
export ARM64_GCC_PATH=$HOME/ti/arm-gnu-toolchain-11.3.rel1-x86_64-aarch64-none-linux-gnu
export CGT7X_ROOT=$HOME/ti/ti-cgt-c7000_4.1.0.LTS
 
TVM_REPO=https://github.com/TexasInstruments/tvm.git
DLR_REPO=https://github.com/TexasInstruments/neo-ai-dlr
 
# Result: Built python package wheels are in {tvm, neo-ai-dlr}/python/dist
 
 
TOP=`pwd`
 
# Build TVM for x86_64
cd $TOP
 git clone --depth 1 --single-branch -b $TVM_TAG $TVM_REPO tvm
 cd tvm
 git submodule update --quiet --init --recursive --depth=1
 mkdir build_x86
 cd build_x86/
# Ubuntu 18.04
# cmake -DUSE_MICRO=ON -DUSE_SORT=ON -DUSE_TIDL=ON -DUSE_LLVM="/cgnas/tvm/deps/clang+llvm-10.0.0-x86_64-linux-gnu-ubuntu-18.04/bin/llvm-config --link-static" -DHIDE_PRIVATE_SYMBOLS=ON -DUSE_TIDL_RT_PATH=$(ls -d ${PSDKR_PATH}/tidl_j7*/arm-tidl/rt) -DUSE_TIDL_PSDKR_PATH=${PSDKR_PATH} -DUSE_CGT7X_ROOT=${CGT7X_ROOT} ..
# Ubuntu 22.04
 cmake -DUSE_MICRO=ON -DUSE_SORT=ON -DUSE_TIDL=ON -DUSE_LLVM="llvm-config --link-static" -DHIDE_PRIVATE_SYMBOLS=ON -DUSE_TIDL_RT_PATH=$(ls -d ${PSDKR_PATH}/${TIDL_DIR_NAME}/arm-tidl/rt) -DUSE_TIDL_PSDKR_PATH=${PSDKR_PATH} -DUSE_CGT7X_ROOT=${CGT7X_ROOT} ..
 make clean
 make -j$(nproc)
 cd ..
 ln -s build_x86 build
 cd python/
 python3 ./setup.py bdist_wheel
 ls dist
 #pip3 uninstall tvm
 #pip3 install dist/tvm-0.9.dev0-cp36-cp36m-linux_x86_64.whl
 
 
# Build DLR for both x86_64 and aarch64
cd $TOP
 git clone --depth 1 --single-branch -b $DLR_TAG $DLR_REPO
 cd neo-ai-dlr
 git submodule update  --quiet --init --recursive --depth=1
 mkdir build_x86
 cd build_x86
 cmake -DUSE_TIDL=ON -DUSE_TIDL_RT_PATH=$(ls -d ${PSDKR_PATH}/${TIDL_DIR_NAME}/arm-tidl/rt) -DDLR_BUILD_TESTS=OFF ..
 make clean
 make -j$(nproc)
 cd ..
 ln -s build_x86 build
 cd python/
 python3 ./setup.py bdist_wheel
 ls dist
 cd dist
 mkdir x86_64
 mv *.whl x86_64
 
 cd ${TOP}/neo-ai-dlrsudo apt install clang llvm
 
TVM_TAG=TIDL_PSDK_9.2.0
DLR_TAG=TIDL_PSDK_9.2.0
 
export PSDKR_PATH=$HOME/psdk_vision/j721e  # SDK build workarea
export TIDL_DIR_NAME=c7x-mma-tidl   # alt: tidl_j7*
export ARM64_GCC_PATH=$HOME/ti/arm-gnu-toolchain-11.3.rel1-x86_64-aarch64-none-linux-gnu
export CGT7X_ROOT=$HOME/ti/ti-cgt-c7000_4.1.0.LTS
 
TVM_REPO=https://github.com/TexasInstruments/tvm.git
DLR_REPO=https://github.com/TexasInstruments/neo-ai-dlr
 
# Result: Built python package wheels are in {tvm, neo-ai-dlr}/python/dist
 
 
TOP=`pwd`
 
# Build TVM for x86_64
cd $TOP
 git clone --depth 1 --single-branch -b $TVM_TAG $TVM_REPO tvm
 cd tvm
 git submodule update --quiet --init --recursive --depth=1
 mkdir build_x86
 cd build_x86/
# Ubuntu 18.04
# cmake -DUSE_MICRO=ON -DUSE_SORT=ON -DUSE_TIDL=ON -DUSE_LLVM="/cgnas/tvm/deps/clang+llvm-10.0.0-x86_64-linux-gnu-ubuntu-18.04/bin/llvm-config --link-static" -DHIDE_PRIVATE_SYMBOLS=ON -DUSE_TIDL_RT_PATH=$(ls -d ${PSDKR_PATH}/tidl_j7*/arm-tidl/rt) -DUSE_TIDL_PSDKR_PATH=${PSDKR_PATH} -DUSE_CGT7X_ROOT=${CGT7X_ROOT} ..
# Ubuntu 22.04
 cmake -DUSE_MICRO=ON -DUSE_SORT=ON -DUSE_TIDL=ON -DUSE_LLVM="llvm-config --link-static" -DHIDE_PRIVATE_SYMBOLS=ON -DUSE_TIDL_RT_PATH=$(ls -d ${PSDKR_PATH}/${TIDL_DIR_NAME}/arm-tidl/rt) -DUSE_TIDL_PSDKR_PATH=${PSDKR_PATH} -DUSE_CGT7X_ROOT=${CGT7X_ROOT} ..
 make clean
 make -j$(nproc)
 cd ..
 ln -s build_x86 build
 cd python/
 python3 ./setup.py bdist_wheel
 ls dist
 #pip3 uninstall tvm
 #pip3 install dist/tvm-0.9.dev0-cp36-cp36m-linux_x86_64.whl
 
 
# Build DLR for both x86_64 and aarch64
cd $TOP
 git clone --depth 1 --single-branch -b $DLR_TAG $DLR_REPO
 cd neo-ai-dlr
 git submodule update  --quiet --init --recursive --depth=1
 mkdir build_x86
 cd build_x86
 cmake -DUSE_TIDL=ON -DUSE_TIDL_RT_PATH=$(ls -d ${PSDKR_PATH}/${TIDL_DIR_NAME}/arm-tidl/rt) -DDLR_BUILD_TESTS=OFF ..
 make clean
 make -j$(nproc)
 cd ..
 ln -s build_x86 build
 cd python/
 python3 ./setup.py bdist_wheel
 ls dist
 cd dist
 mkdir x86_64
 mv *.whl x86_64
 
 cd ${TOP}/neo-ai-dlr
 mkdir build_aarch64
 cd build_aarch64
 cmake -DUSE_TIDL=ON -DUSE_TIDL_RT_PATH=$(ls -d ${PSDKR_PATH}/${TIDL_DIR_NAME}/arm-tidl/rt) -DDLR_BUILD_TESTS=OFF -DCMAKE_TOOLCHAIN_FILE=../cmake/ti-aarch64-linux-gcc-toolchain.cmake ..
 make clean
 make -j$(nproc)
 cd ..
 rm -f build
 ln -s build_aarch64 build
 cd python/
 python3 ./setup.py bdist_wheel
 ls dist
 cd dist
 mkdir aarch64
 mv *.whl aarch64

 mkdir build_aarch64
 cd build_aarch64
 cmake -DUSE_TIDL=ON -DUSE_TIDL_RT_PATH=$(ls -d ${PSDKR_PATH}/${TIDL_DIR_NAME}/arm-tidl/rt) -DDLR_BUILD_TESTS=OFF -DCMAKE_TOOLCHAIN_FILE=../cmake/ti-aarch64-linux-gcc-toolchain.cmake ..
 make clean
 make -j$(nproc)
 cd ..
 rm -f build
 ln -s build_aarch64 build
 cd python/
 python3 ./setup.py bdist_wheel
 ls dist
 cd dist
 mkdir aarch64
 mv *.whl aarch64
